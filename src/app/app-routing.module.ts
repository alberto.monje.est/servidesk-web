import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetalleTicketCordinadorComponent } from './components/detalle-ticket-cordinador/detalle-ticket-cordinador.component';
import { DetalleTicketTenicoComponent } from './components/detalle-ticket-tenico/detalle-ticket-tenico.component';
import { ListaCatalogoUsuarioComponent } from './components/lista-catalogo-usuario/lista-catalogo-usuario.component';
import { ListaTicketCordinadorComponent } from './components/lista-ticket-cordinador/lista-ticket-cordinador.component';
import { ListaTicketTecnicoComponent } from './components/lista-ticket-tecnico/lista-ticket-tecnico.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrarCoordinadorComponent } from './components/registrar-coordinador/registrar-coordinador.component';
import { RegistrarTecnicoComponent } from './components/registrar-tecnico/registrar-tecnico.component';
import { RegistrarTicketComponent } from './components/registrar-ticket/registrar-ticket.component';
import { RegistrarUsuarioComponent } from './components/registrar-usuario/registrar-usuario.component';
import { ReportesComponent } from './components/reportes/reportes.component';
import { SolicitarServicioComponent } from './components/solicitar-servicio/solicitar-servicio.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { GenerarTicketComponent } from './components/generar-ticket/generar-ticket.component';
import { RegistrarPreguntasComponent } from './components/registrar-preguntas/registrar-preguntas.component';
import { ListadoTicketUsuarioComponent } from './components/listado-ticket-usuario/listado-ticket-usuario.component';
import { EncuestaComponent } from './components/encuesta/encuesta.component';
import { ListaCatalogosUsuarioComponent } from './components/lista-catalogos-usuario/lista-catalogos-usuario.component';
import { ListaServiciosUsuarioComponent } from './components/lista-servicios-usuario/lista-servicios-usuario.component';


const routes: Routes = [

  {path:'' ,redirectTo:'login',pathMatch:'full'},
  {path:'login', component: LoginComponent },
  {path:'ListaCatalogoUsuario', component: ListaCatalogoUsuarioComponent },
  {path:'ListaTicketCordinador', component: ListaTicketCordinadorComponent },
  {path:'detalleTicketCordinador', component: DetalleTicketCordinadorComponent },
  {path:'detalleTicketCordinador/{codticket}', component: DetalleTicketCordinadorComponent },
  {path:'detalleTicketTenico', component: DetalleTicketTenicoComponent },
  {path:'listatickettecnico', component: ListaTicketTecnicoComponent },
  {path:'registrarTecnico', component: RegistrarTecnicoComponent },
  {path:'registrarTicket', component: RegistrarTicketComponent },
  {path:'registrarCoordinador', component: RegistrarCoordinadorComponent },
  {path:'registrarUsuario', component: RegistrarUsuarioComponent},
  {path:'reportes', component: ReportesComponent},
  {path:'servicio', component: SolicitarServicioComponent },
  {path:'catalogo',component:CatalogoComponent},
  {path:'generarTicket',component:GenerarTicketComponent},
  {path:'preguntas',component:RegistrarPreguntasComponent},
  {path:'listadoTicketUsuario',component:ListadoTicketUsuarioComponent},
  {path:'encuesta',component:EncuestaComponent},
  {path:'listaservicios',component:ListaServiciosUsuarioComponent},
  {path:'listacatalogos',component:ListaCatalogosUsuarioComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


