import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CabeceraComponent } from './components/cabecera/cabecera.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrarTicketComponent } from './components/registrar-ticket/registrar-ticket.component';
import { RegistrarUsuarioComponent } from './components/registrar-usuario/registrar-usuario.component';
import { RegistrarTecnicoComponent } from './components/registrar-tecnico/registrar-tecnico.component';
import { ListaCatalogoUsuarioComponent } from './components/lista-catalogo-usuario/lista-catalogo-usuario.component';
import { SolicitarServicioComponent } from './components/solicitar-servicio/solicitar-servicio.component';
import { DetalleTicketUsuarioComponent } from './components/detalle-ticket-usuario/detalle-ticket-usuario.component';
import { ListaTicketTecnicoComponent } from './components/lista-ticket-tecnico/lista-ticket-tecnico.component';
import { ListaTicketCordinadorComponent } from './components/lista-ticket-cordinador/lista-ticket-cordinador.component';
import { DetalleTicketCordinadorComponent } from './components/detalle-ticket-cordinador/detalle-ticket-cordinador.component';
import { DetalleTicketTenicoComponent } from './components/detalle-ticket-tenico/detalle-ticket-tenico.component';
import { ReportesComponent } from './components/reportes/reportes.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdown} from '@ng-bootstrap/ng-bootstrap';
import { RegistrarCoordinadorComponent } from './components/registrar-coordinador/registrar-coordinador.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GenerarTicketComponent } from './components/generar-ticket/generar-ticket.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { RegistrarPreguntasComponent } from './components/registrar-preguntas/registrar-preguntas.component';
import { ListadoTicketUsuarioComponent } from './components/listado-ticket-usuario/listado-ticket-usuario.component';
import { EncuestaComponent } from './components/encuesta/encuesta.component';
import { ListaServiciosUsuarioComponent } from './components/lista-servicios-usuario/lista-servicios-usuario.component';
import { ListaCatalogosUsuarioComponent } from './components/lista-catalogos-usuario/lista-catalogos-usuario.component';



@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    LoginComponent,
    RegistrarTicketComponent,
    RegistrarUsuarioComponent,
    RegistrarTecnicoComponent,
    ListaCatalogoUsuarioComponent,
    SolicitarServicioComponent,
    DetalleTicketUsuarioComponent,
    ListaTicketTecnicoComponent,
    ListaTicketCordinadorComponent,
    DetalleTicketCordinadorComponent,
    DetalleTicketTenicoComponent,
    ReportesComponent,
    RegistrarCoordinadorComponent,
    CatalogoComponent,
    GenerarTicketComponent,
    RegistrarPreguntasComponent,
    CatalogoComponent,
    ListadoTicketUsuarioComponent,
    EncuestaComponent,
    ListaServiciosUsuarioComponent,
    ListaCatalogosUsuarioComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,

    // ToastrModule.forRoot({
    //   timeOut:2000,
    //   progressBar:true,
    //   progressAnimation: 'increasing',
    //   preventDuplicates:true
    // })

  ],
  providers: [NgbActiveModal, NgbDropdown],
  bootstrap: [AppComponent]
})
export class AppModule { }
