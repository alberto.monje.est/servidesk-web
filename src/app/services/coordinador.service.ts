import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Coordinador } from '../models/coordinador';

@Injectable({
  providedIn: 'root'
})
export class CoordinadorService {

  url = 'http://servi-desk.herokuapp.com/coordinador/';

  urlApi(metodo: string) {
    const url = `http://servi-desk.herokuapp.com/coordinador/${metodo}`
    return url;
  }
  constructor(private http: HttpClient) { }


  createCoordinador(coordinador: Coordinador) {
    const headers = { "content-type": "application/json" };
    let body = {
      
      "email": coordinador.email.toString(),
      "password": coordinador.password.toString(),
      "persona": {
        "cedula": coordinador.persona.cedula.toString()
      }
    }
    return this.http.post(this.url + "guardar-coordinador", body, {
      headers: headers, observe: "response"
    })
      .toPromise();
  }

  editarCoordinador(id:number ,coordinador: Coordinador) {
    const headers = { "content-type": "application/json" };
    let body = {
      "codCoordinador": coordinador.codCoordinador.toString(),
      "email": coordinador.email.toString(),
      "password": coordinador.password.toString(),
    }
    return this.http.put(this.url + "actualizar-coordinador/"+id, body, {
      headers: headers, observe: "response"
    })
      .toPromise();
  }

  getCoordinador(id: number): Observable<Coordinador> {
    const url = `${this.urlApi("buscar-coordinador/")}${id}`
    return this.http.get<Coordinador>(url);
  }

}