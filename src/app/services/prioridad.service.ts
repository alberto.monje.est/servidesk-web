import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable} from 'rxjs';
import { Prioridad } from '../models/prioridad';

@Injectable({
  providedIn: 'root'
})
export class PrioridadService {

  constructor(private http: HttpClient) { }

  urlApi(metodo:string){
    const url=`http://servi-desk.herokuapp.com/prioridad/${metodo}`
    return url;
  }

  getAllPrioridad():Observable<Prioridad[]>{
    return this.http.get<Prioridad[]>(this.urlApi("listar"));
  }

  getPrioridad(codPrioridad:number):Observable<Prioridad>{
    const url=`${this.urlApi("buscar/")}${codPrioridad}`
    return this.http.get<Prioridad>(url)
  }
}