import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable} from 'rxjs';
import { Preguntas } from '../models/preguntas';
import { Catalogo } from '../models/catalogo';

@Injectable({
  providedIn: 'root'
})
export class PreguntasService {

  constructor(private http: HttpClient) { }

  urlApi(metodo:string){
    const url=`http://servi-desk.herokuapp.com/preguntas/${metodo}`
    return url;
  }

  addNewPregunta(pregunta:Preguntas):Observable<Preguntas>{
    return this.http.post<Preguntas>(this.urlApi("guardar"),pregunta)
  }

  getAllPreguntas():Observable<Preguntas[]>{
    return this.http.get<Preguntas[]>(this.urlApi("listar"))
  }

  deletePregunta(codPregunta:number):Observable<any>{
    const url =`${this.urlApi("delete/")}${codPregunta}`
    return this.http.delete(url)
  }

  updatePregunta(codPregunta:number,pregunta:Preguntas):Observable<Preguntas>{
    const url =`${this.urlApi("actualizar/")}${codPregunta}`
    return this.http.put<Preguntas>(url,pregunta)
  }

  getPreguntaByCodigo(codPregunta:number):Observable<Preguntas>{
    const url =`${this.urlApi("buscar/")}${codPregunta}`
    return this.http.get<Preguntas>(url)
  }
}
