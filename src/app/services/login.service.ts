import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) { }
  url = 'http://servi-desk.herokuapp.com/';
  getDataForLoginFlat = () => {
    let data = {
        "logo": "assets/images/csform-logo.png",
        "btnLogin": "Login",
        "txtUsername" : "Username",
        "txtPassword" : "Password",
        "txtForgotPassword" : "Forgot password?",
        "btnResetYourPassword": "Reset your password",
        "txtSignupnow" : "Don't have an account?",
        "btnSignupnow": "Signup now",
        "title": "Welcome back,",
        "subtitle": "please login to your account.",
        "errorUser" : "Field can't be empty.",
        "errorPassword" : "Field can't be empty."
    };
    return data;
};
loginTecnico(email:string,contrasena:string){
  return new Promise(resolve => {
    this.http.get(this.url+'/tecnico/login/'+email+'/'+contrasena).subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}
loginUsuario(email:string,contrasena:string){
  return new Promise(resolve => {
    this.http.get(this.url+'/usuario/login/'+email+'/'+contrasena).subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}
loginCoordinador(email:string,contrasena:string){
  return new Promise(resolve => {
    this.http.get(this.url+'/coordinador/login/'+email+'/'+contrasena).subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}
}