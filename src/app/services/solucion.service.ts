import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Solucion } from '../models/solucion';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SolucionService {

  constructor(private http: HttpClient) { }
  private urlapi = 'https://servi-desk.herokuapp.com/solucion';
  urlApi(metodo: string) {
    const url = `https://servi-desk.herokuapp.com/solucion/${metodo}`
    return url;
  }


  crearSolucion(solucion: Solucion): Observable<Solucion>{
    return this.http.post<Solucion>(this.urlapi + '/guardar', solucion);
    
  }


  getTicketById(id:number) {
    return new Promise(resolve => {
      this.http.get(this.urlapi+'/buscar_codticket/'+id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getTicket(codTicket: number): Observable<Solucion> {
    const url = `${this.urlApi("buscar_codticket/")}${codTicket}`
    return this.http.get<Solucion>(url);
  }

}
