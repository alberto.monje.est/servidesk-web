import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Severidad } from '../models/severidad';

@Injectable({
  providedIn: 'root'
})
export class SeveridadService {
url='http://servi-desk.herokuapp.com/';
  constructor(private http: HttpClient) {
  }

  private urlapi = 'http://servi-desk.herokuapp.com/severidad/listar-severidades';
  private urlapo = 'http://servi-desk.herokuapp.com/severidad/recuperar-severidad';

  getAllSeveridad(): Observable<Severidad[]> {
      return this.http.get<Severidad[]>(this.urlapi);
      
      } 

      getSeveridad(id: number): Observable<any> {
        return this.http.get(`${this.urlapo}/${id}`);
    }

    createSeveridad(severidad: Object): Observable<Object> {
        return this.http.post(`${this.urlapo}`, severidad);
    }

    updateSeveridad(id: number, value: any): Observable<Object> {
        return this.http.put(`${this.urlapi}/${id}`, value);
}

getListSeveridad() {
  return new Promise(resolve => {
    this.http.get(this.url+'severidad/listar-severidades').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}
}