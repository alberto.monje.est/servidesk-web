import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url = 'http://servi-desk.herokuapp.com/usuario/';

  urlApi(metodo: string) {
    const url = `http://servi-desk.herokuapp.com/usuario/${metodo}`
    return url;
  }
  constructor(private http: HttpClient) { }


  createUsuario(usuario: Usuario) {
    const headers = { "content-type": "application/json" };
    let body = {
      
      "email": usuario.email.toString(),
      "contrasena": usuario.contrasena.toString(),
      "persona": {
        "cedula": usuario.persona.cedula.toString()
      }
    }
    return this.http.post(this.url + "crear", body, {
      headers: headers, observe: "response"
    })
      .toPromise();
  }

  editarUsuario(id:number ,usuario: Usuario) {
    const headers = { "content-type": "application/json" };
    let body = {
      "codUsuario": usuario.codUsuario.toString(),
      "email": usuario.email.toString(),
      "contrasena": usuario.contrasena.toString(),
    }
    return this.http.put(this.url + "editar/"+id, body, {
      headers: headers, observe: "response"
    })
      .toPromise();
  }

  getUsuario(id: number): Observable<Usuario> {
    const url = `${this.urlApi("buscar/")}${id}`
    return this.http.get<Usuario>(url);
  }

}
