import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable} from 'rxjs';
import { Criticidad } from '../models/criticidad';

@Injectable({
  providedIn: 'root'
})
export class CriticidadService {

  constructor(private http: HttpClient) { }

  urlApi(metodo:string){
    const url=`http://servi-desk.herokuapp.com/criticidad/${metodo}`
    return url;
  }

  getAllCriticidad():Observable<Criticidad[]>{
    return this.http.get<Criticidad[]>(this.urlApi("listar"))
  }

  getCriticidad(codCriticidad:number):Observable<Criticidad>{
    const url=`${this.urlApi("buscarporcodigo/")}${codCriticidad}`
    return this.http.get<Criticidad>(url)
  }
}
