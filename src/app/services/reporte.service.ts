import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable} from 'rxjs';
import { ReporteGeneral } from '../models/reporteDto';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  constructor(private http: HttpClient) { }

  getAllReportes(fechainicial:Date,fechafinal:Date,codEstado:number):Observable<ReporteGeneral[]>{
    const url=`http://servi-desk.herokuapp.com/reportes/listar?fechainicial=${fechainicial}&fechafinal=${fechafinal}&codestado=${codEstado}`
    return this.http.get<ReporteGeneral[]>(url)
  }

  exportCsv(fechainicial:Date,fechafinal:Date,codEstado:number):Observable<any>{
    const headers = new HttpHeaders().set('Content-Type','application/json')
    const url=`http://servi-desk.herokuapp.com/reportes/exportar-csv?fechainicial=${fechainicial}&fechafinal=${fechafinal}&codestado=${codEstado}`
    return this.http.get<ReporteGeneral[]>(url,{headers,responseType:'blob' as 'json'})
  }
}
