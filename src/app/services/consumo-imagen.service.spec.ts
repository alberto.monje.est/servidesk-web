import { TestBed } from '@angular/core/testing';

import { ConsumoImagenService } from './consumo-imagen.service';

describe('ConsumoImagenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsumoImagenService = TestBed.get(ConsumoImagenService);
    expect(service).toBeTruthy();
  });
});
