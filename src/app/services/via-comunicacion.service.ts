import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ViaComunicacion } from '../models/viaComunicacion';

@Injectable({
  providedIn: 'root'
})
export class ViaComunicacionService {
  urls = 'http://servi-desk.herokuapp.com/viaComunicacion/';
  constructor(private http: HttpClient) { }

  urlApi(metodo:string){
    const url=`http://servi-desk.herokuapp.com/viaComunicacion/${metodo}`
    return url;
  }

  getAllViaComunicacion():Observable<ViaComunicacion[]>{
    return this.http.get<ViaComunicacion[]>(this.urlApi("listar"))
  }

  getViaComunicacion(codTipo:number):Observable<ViaComunicacion>{
    const url=`${this.urlApi("buscar/")}${codTipo}`
    return this.http.get<ViaComunicacion>(url)
  }

  getViaComunicaciona() {
    return new Promise(resolve => {
      this.http.get(this.urls+'listar').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
}