import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Persona } from '../models/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  private baseUrl = 'http://servi-desk.herokuapp.com/persona/';

  urlApi(metodo: string) {
    const url = `http://servi-desk.herokuapp.com/persona/${metodo}`
    return url;
  }


  constructor(private http: HttpClient) { }

  getPersona(id: string): Observable<Persona> {
    const url = `${this.urlApi("buscar/")}${id}`
    return this.http.get<Persona>(url);
  }
  createPersona(Persona: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, Persona);
  }

  updatePersona(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deletePersona(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getPersonaList(): Observable<any> {
    return this.http.get(`${this.baseUrl+'/listar'}`);
  }


  getcreatePersona(persona: Persona) {
    const headers = { "content-type": "application/json" };
    let body = {
      "cedula": persona.cedula.toString(),
      "nombres": persona.nombres.toString(),
      "telefono": persona.telefono.toString(),
      "direccion": persona.direccion.toString()
    }
    return this.http.post(this.baseUrl + "guardar", body, {
      headers: headers, observe: "response"
    })
      .toPromise();
  }

  editarPersona(id: string, persona: Persona) {
    const headers = { "content-type": "application/json" };
    let body = {
      "cedula": persona.cedula.toString(),
      "nombres": persona.nombres.toString(),
      "telefono": persona.telefono.toString(),
      "direccion": persona.direccion.toString()
    }
    return this.http.put(this.baseUrl + "editar/"+id, body, {
      headers: headers, observe: "response"
    })
      .toPromise();
  }

}
