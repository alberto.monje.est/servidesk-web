import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable} from 'rxjs';
import { Tipo } from '../models/tipo';

@Injectable({
  providedIn: 'root'
})
export class TipoService {

  constructor(private http: HttpClient) { }

  urlApi(metodo:string){
    const url=`http://servi-desk.herokuapp.com/tipo/${metodo}`
    return url;
  }

  getAllTipo():Observable<Tipo[]>{
    return this.http.get<Tipo[]>(this.urlApi("listar"))
  }

  getTipo(codTipo:number):Observable<Tipo>{
    const url=`${this.urlApi("buscar/")}${codTipo}`
    return this.http.get<Tipo>(url)
  }
}