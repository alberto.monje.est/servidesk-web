import { TestBed } from '@angular/core/testing';

import { ViaComunicacionService } from './via-comunicacion.service';

describe('ViaComunicacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViaComunicacionService = TestBed.get(ViaComunicacionService);
    expect(service).toBeTruthy();
  });
});
