import { TestBed } from '@angular/core/testing';

import { SeveridadService } from './severidad.service';

describe('SeveridadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SeveridadService = TestBed.get(SeveridadService);
    expect(service).toBeTruthy();
  });
});
