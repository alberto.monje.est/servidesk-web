import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable} from 'rxjs';
import { Catalogo } from '../models/catalogo';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  url='http://servi-desk.herokuapp.com/catalogo/'
  constructor(private http: HttpClient) { }

  urlApi(metodo:string){
    const url=`http://servi-desk.herokuapp.com/catalogo/${metodo}`
    return url;
  }

  addNewCatalogo(catalogo:Catalogo):Observable<Catalogo>{
    return this.http.post<Catalogo>(this.urlApi("guardar"),catalogo);
  }

  getAllCatalogo():Observable<Catalogo[]>{
    return this.http.get<Catalogo[]>(this.urlApi("listar"))
  }

  getCatalogoByCategoria(categoria:String):Observable<Catalogo>{
    const url=`${this.urlApi("buscarporcategoria/")}${categoria}`
    return this.http.get<Catalogo>(url);
  }

  deleteCatalogo(codCatalogo:number):Observable<any>{
    const url =`${this.urlApi("eliminar/")}${codCatalogo}`
    return this.http.delete(url)
  }

  putCatalogo(codCatalogo:number,catalogo:Catalogo):Observable<Catalogo>{
    const url =`${this.urlApi("actualizarcatalogo/")}${codCatalogo}`
    return this.http.put<Catalogo>(url,catalogo)
  }

  getCatalogoByCodigo(codCatalogo:number):Observable<Catalogo>{
    const url =`${this.urlApi("buscarporcodigo/")}${codCatalogo}`
    return this.http.get<Catalogo>(url);
  }

  getCatalogo() {
    return new Promise(resolve => {
      this.http.get(this.url+'listar').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}