import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tecnico } from '../models/tecnico';

@Injectable({
  providedIn: 'root'
})
export class TecnicoService {

  constructor(private http: HttpClient) { }


  private urlapi = 'http://servi-desk.herokuapp.com/tecnico/';
  
  


  urlApi(metodo: string) {
    const url = `http://servi-desk.herokuapp.com/tecnico/${metodo}`
    return url;
  }

  getAllTecnicos(): Observable<Tecnico[]> {
      return this.http.get<Tecnico[]>(this.urlapi+'listar');
      
      } 

createTecnico(tecnico: Tecnico) {
  const headers = { "content-type": "application/json" };
  let body = {
    
    "email": tecnico.email.toString(),
    "contrasena": tecnico.contrasena.toString(),
    "nivel": tecnico.nivel,
    "persona": {
      "cedula": tecnico.persona.cedula.toString()
    }
  }
  return this.http.post(this.urlapi + "guardar", body, {
    headers: headers, observe: "response"
  })
    .toPromise();
}

editarTecnico(id:number ,tecnico: Tecnico) {
  const headers = { "content-type": "application/json" };
  let body = {
    "codTecnico": tecnico.codTecnico,
    "email": tecnico.email.toString(),
    "contrasena": tecnico.contrasena.toString(),
    "nivel": tecnico.nivel,
  }
  return this.http.put(this.urlapi + "actualizartecnico/"+id, body, {
    headers: headers, observe: "response"
  })
    .toPromise();
}

getTecnico(id: number): Observable<Tecnico> {
  const url = `${this.urlApi("tecnico/")}${id}`
  return this.http.get<Tecnico>(url);
}
obtenerTecnicoNivel(nivel: number): Observable<any>{
  return this.http.get<any>(this.urlapi + '/listar-tecnicos-niveles/'+ nivel)
}

}