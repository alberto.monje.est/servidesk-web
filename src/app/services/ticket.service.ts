import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { resolve } from 'url';
import { Estado } from '../models/estado';
import { Ticket } from '../models/ticket';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  ticketUrl: any = "";

  constructor(private http: HttpClient) {
  }

  private urlapi = 'http://servi-desk.herokuapp.com/ticket';
  

  urlApi(metodo: string) {
    const url = `http://servi-desk.herokuapp.com/ticket/${metodo}`
    return url;
  }

  getAllTickets(): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(this.urlapi + '/listar');

  }

  getTicket(codTicket: number): Observable<Ticket> {
    const url = `${this.urlApi("listar/")}${codTicket}`
    return this.http.get<Ticket>(url);
  }


  updateAsignarTecnico(cod_tecnico: number, cod_coordinador: number, cod_ticket: number, value: any): Observable<Object> {
    return this.http.put(`${this.urlapi + '/asignarTecnico'}/${cod_tecnico}/${cod_coordinador}/${cod_ticket}`, value);
  }

  updateCambiarEstado(cod_estado: number, cod_ticket: number, value: any): Observable<Object> {
    return this.http.put(`${this.urlapi + '/cambiar_estado'}/${cod_estado}/${cod_ticket}`, value);
  }

  updateFechaAsigancion(cod_tecnico: number, value: any): Observable<Object> {
    return this.http.put(`${this.urlapi + '/fecha_asignacion'}/${cod_tecnico}`, value);
  }

  updateSeveridad(cod_severidad: number, cod_ticket: number, value: any): Observable<Object> {
    return this.http.put(`${this.urlapi + '/cambiar_severidad'}/${cod_severidad}/${cod_ticket}`, value);
  }
  getTicketByIdTecnico(codTecnico: number): Observable<any[]> {
        return this.http.get<any[]>(this.urlapi + '/tecnico/'+ codTecnico)
      }

  putTicketEstado(codEstado: number, codTicket: number): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this.http.put(this.urlapi + '/cambiar_estado/' + codEstado + '/' + codTicket, null, options);
  }

  getTicketByIdTicket(codTicket: number): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(this.urlapi + '/listar/' + codTicket);
  }

  escalarTicket(codTecnico: number, codCoordinador: number, codTicket: number): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this.http.put(this.urlapi + '/asignarTecnico/' + codTecnico + '/' + codCoordinador + '/' + codTicket, null, options)
  }

  reasignarTicket(codTicket: number): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this.http.put(this.urlapi + '/reasignar/' + codTicket, null, options)
  }
  createTicket(ticket: Ticket) {
    const headers = { "content-type": "application/json" };
    this.ticketUrl = ticket.url;
    console.log("url en ticket service" + this.ticketUrl);
    let body = {

      "descripcionTicket": ticket.descripcionTicket.toString(),
      "estado": {
        "codEstado": ticket.estado
      },
      "fechaCreacion": ticket.fechaCreacion.toString(),
      "fechaAsignacion": ticket.fechaAsignacion.toString(),
      "ticketEstado": ticket.ticketEstado.valueOf(),
      "confirmacion": ticket.confirmacion.valueOf(),
      "sla": ticket.sla.toString(),
      "servicio": {
        "codServicio": ticket.codServicio
      },
      "severidad": {
        "codSeveridad": ticket.codSeveridad
      },
      "url": this.ticketUrl,
      "usuario": {
        "codUsuario": ticket.codUsuario
      },
      "viaComunicacion": {
        "codViaComunicacion": ticket.codViaComunicacion
      }
    }
    return this.http
      .post(this.urlapi + "/guardar", body, {
        headers: headers,
        observe: "response"
      })
      .toPromise();
  }

  updateTicket(ticket: Ticket) {
    const headers = { "content-type": "application/json" };
    this.ticketUrl = ticket.url;
    console.log("url en ticket service" + this.ticketUrl);
    let body = {
      "codticket": ticket.codticket.toString(),
      "descripcionTicket": ticket.descripcionTicket.toString(),
      "estado": {
        "codEstado": ticket.estado
      },
      "fechaCreacion": ticket.fechaCreacion.toString(),
      "fechaAsignacion": ticket.fechaAsignacion.toString(),
      "ticketEstado": ticket.ticketEstado.valueOf(),
      "confirmacion": ticket.confirmacion.valueOf(),
      "sla": ticket.sla.toString(),
      "servicio": {
        "codServicio": ticket.codServicio
      },
      "severidad": {
        "codSeveridad": ticket.codSeveridad
      },
      "url": this.ticketUrl,
      "usuario": {
        "codUsuario": ticket.codUsuario
      },
      "viaComunicacion": {
        "codViaComunicacion": ticket.codViaComunicacion
      }
    }
    return this.http
      .post(this.urlapi + "/guardar", body, {
        headers: headers,
        observe: "response"
      })
      .toPromise();
  }
     confirmarTicket(codTicket:number) : Observable<any>{
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers};
      return this.http.put(this.urlapi + '/confirmar/'+codTicket, null, options)
     }
      



  getTicketByTipoServicio(tipo: string) {
    return new Promise(resolve => {
      this.http.get(this.urlapi + '/tipo_servicio/' + tipo).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  CambioDeEstadoTicket(id: number, estado: boolean) {
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers };
      this.http.put(this.urlapi + 'eliminar_recuperar/' + estado + '/' + id, null, options).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getTicketByEliminados_Activos(booleano: boolean) {
    return new Promise(resolve => {
      this.http.get(this.urlapi + 'listar_eliminados/' + booleano).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getTicketById(id: number) {
    return new Promise(resolve => {
      this.http.get(this.urlapi + '/listar/' + id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getAllTicketsUsuario(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.urlapi + '/listar');
  }
  getAllEstados(): Observable<Estado[]> {
      return this.http.get<Estado[]>(this.urlapi + '/listar');
  }

  CambioSla(sla:number, id: number) {
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers };
      this.http.put(this.urlapi + '/sla/' + sla + '/' + id, null, options).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getTicketsByUser(codUsuario:number) {
    return new Promise(resolve => {
      this.http.get(this.urlapi+'/usuario/'+codUsuario).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getTicketsCoordinador() {
    return new Promise(resolve => {
      this.http.get(this.urlapi+'/ticketCoordinador').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getTicketsByestado(codEstado:number) {
    return new Promise(resolve => {
      this.http.get(this.urlapi+'/ticketestado/'+codEstado).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
putCambiarEstado(cod_estado:number, cod_ticket: number){
    return new Promise(resolve =>{
      let headers = new HttpHeaders({ 'Content-Type':'application/json'});
      let options = {headers: headers};
      this.http.put(this.urlapi+'/cambiar_estado/'+cod_estado+'/'+cod_ticket,null,options).subscribe(data =>{
        resolve(data);
      },err =>{
        console.log(err);
      });
    });
  }
  
}









