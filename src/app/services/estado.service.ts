import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Estado } from '../models/estado';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  constructor( private http: HttpClient) { }

  private urlapi =  'http://servi-desk.herokuapp.com/estado/listar-estados'
  getAllEstados():Observable<Estado[]>{
    return this.http.get<Estado[]>(this.urlapi);
  }
}
