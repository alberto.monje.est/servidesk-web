import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable} from 'rxjs';
import { Servicio } from '../models/servicio';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  constructor(private http: HttpClient) { }

  urlApi(metodo:string){
    const url=`http://servi-desk.herokuapp.com/servicio/${metodo}`
    return url;
  }
  urlapi2 = 'http://servi-desk.herokuapp.com/servicio/';

  addNewServicio(servicio:Servicio):Observable<Servicio>{
    return this.http.post<Servicio>(this.urlApi("guardar-servicio"),servicio)

  }

  getAllServicios():Observable<Servicio[]>{
    return this.http.get<Servicio[]>(this.urlApi("listar-servicio"))
  }

  deleteServicio(codServicio:number):Observable<any>{
    const url =`${this.urlApi("eliminar-servicio/")}${codServicio}`
    return this.http.delete(url)
  }

  updateServicio(servicio:Servicio,codServicio:number):Observable<Servicio>{
    const url =`${this.urlApi("actualizar-servicio/")}${codServicio}`
    return this.http.put<Servicio>(url,servicio)
  }

  getServicioByCodigo(codServicio:number):Observable<Servicio>{
    const url =`${this.urlApi("buscar-servicio/")}${codServicio}`
    return this.http.get<Servicio>(url)
  }

  getServicioPage(codCatalogo:number,page:number,size:number):Observable<Servicio[]>{
    const url =`${this.urlApi("listaservicios/")}${codCatalogo}/${page}/${size}`
    return this.http.get<Servicio[]>(url)
  }

  getServicioByCategoria(codCatalogo:number):Observable<Servicio[]>{
    const url =`${this.urlApi("listaserviciosporcategoria/")}${codCatalogo}`
    return this.http.get<Servicio[]>(url)
  }

  getServiciosByTipo(codTipo:number, codCatalogo:number  ): Observable<any>{
    return this.http.get(`${this.urlapi2}filtrar-servicio/`+ codTipo +'/'+ codCatalogo);
  }
}
