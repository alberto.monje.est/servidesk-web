import { TestBed } from '@angular/core/testing';

import { DetalleEncuestaService } from './detalle-encuesta.service';

describe('DetalleEncuestaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetalleEncuestaService = TestBed.get(DetalleEncuestaService);
    expect(service).toBeTruthy();
  });
});
