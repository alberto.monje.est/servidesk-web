import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleTicketTenicoComponent } from './detalle-ticket-tenico.component';

describe('DetalleTicketTenicoComponent', () => {
  let component: DetalleTicketTenicoComponent;
  let fixture: ComponentFixture<DetalleTicketTenicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleTicketTenicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleTicketTenicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
