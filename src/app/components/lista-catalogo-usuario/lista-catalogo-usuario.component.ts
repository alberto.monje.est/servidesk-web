import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Tipo } from 'src/app/models/tipo';
import { TipoService } from 'src/app/services/tipo.service';

@Component({
  selector: 'app-lista-catalogo-usuario',
  templateUrl: './lista-catalogo-usuario.component.html',
  styleUrls: ['./lista-catalogo-usuario.component.css']
})
export class ListaCatalogoUsuarioComponent implements OnInit {
  tipos: Observable<Tipo[]>;
  constructor(private tipoService: TipoService, private  router: Router ) {
  }

   ngOnInit() {
    this.listar();
  }
 listar() {
  this.tipos = this.tipoService.getAllTipo();
}

cambioPantalla(tomarvalor:string){
  localStorage.setItem("codTipos",tomarvalor)
   this.router.navigateByUrl("/generarTicket");
 }
}
