import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCatalogoUsuarioComponent } from './lista-catalogo-usuario.component';

describe('ListaCatalogoUsuarioComponent', () => {
  let component: ListaCatalogoUsuarioComponent;
  let fixture: ComponentFixture<ListaCatalogoUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCatalogoUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCatalogoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
