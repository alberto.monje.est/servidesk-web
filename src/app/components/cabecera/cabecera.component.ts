import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegistrarCoordinadorComponent } from '../registrar-coordinador/registrar-coordinador.component';
import { RegistrarTecnicoComponent } from '../registrar-tecnico/registrar-tecnico.component';
import { RegistrarUsuarioComponent } from '../registrar-usuario/registrar-usuario.component';

@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit {
  @Input()
  dato
  nombre
  ref
  constructor(private modalService: NgbModal) {
    this.dato = localStorage.getItem("validaMenu");
    this.nombre = localStorage.getItem("nombreUsuario");
  }

  ngOnInit() {

  }

  cerrarSesion() {
    localStorage.removeItem("validaMenu")
    localStorage.removeItem("nombreUsuario")
  }

  Editarticket() {
    if (this.dato == 1) {
      this.ref = this.modalService.open(RegistrarUsuarioComponent, { centered: true });
    }
    if (this.dato == 2) {
      this.ref = this.modalService.open(RegistrarTecnicoComponent, { centered: true });
    }
    if (this.dato == 3) {
      this.ref = this.modalService.open(RegistrarCoordinadorComponent, { centered: true });
    }
   
    this.ref.result.then((yes) => {
      console.log("Yes Click");

    },
      (cancel) => {
        console.log("Cancel Click");

      })
  }
  modalcoordinar(){
    this.ref = this.modalService.open(RegistrarCoordinadorComponent, { centered: true });
    this.ref.result.then((yes) => {
      console.log("Yes Click");

    },
      (cancel) => {
        console.log("Cancel Click");

      })
  }

  modaltecnico(){
    this.ref = this.modalService.open(RegistrarTecnicoComponent, { centered: true });
    this.ref.result.then((yes) => {
      console.log("Yes Click");

    },
      (cancel) => {
        console.log("Cancel Click");

      })
  }
 
  modalUsuario(){
    this.ref = this.modalService.open(RegistrarUsuarioComponent, { centered: true });
    this.ref.result.then((yes) => {
      console.log("Yes Click");

    },
      (cancel) => {
        console.log("Cancel Click");

      })
  }


}
