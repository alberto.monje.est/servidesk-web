import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegistrarUsuarioComponent } from '../registrar-usuario/registrar-usuario.component';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { Tecnico } from 'src/app/models/tecnico';
import { Usuario } from 'src/app/models/usuario';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  titular: string = '';
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  @Input() redirectTo: string = "/home";
  public username: string;
  public password: string;
  longusuario: any = [];
  longtecnico: any = [];
  longcoordinador: any = [];
  usuarios = [];
  @Input() data: any;
  @Input() events: any;
  ///tomadin el valor para el menu
  valorMenu: string
  NombreLogiadoCoordinador: any = [];
  NombreLogiadoUsuario: any = [];
  NombreLogiadoTecnico: any = [];
  tomardato: string
  /// dato a llenar
  cedulaPersona: string
  codigoUsuario: string


  constructor(private modalService: NgbModal, private router: Router, private loginServicio: LoginService) {
   
  }
  ngOnInit() {
  }

  modelREsgistrar() {
    const ref = this.modalService.open(RegistrarUsuarioComponent, { centered: true })
    ref.result.then((yes) => {
      console.log("Yes Click");

    },
      (cancel) => {
        console.log("Cancel Click");

      })
  }

  ingresar() {
    this.consultaValidacionUserPasword();
  }

  solousuario(){

  }
  validarUsuariosMultiples() {

    if (this.longtecnico == null && this.longcoordinador == null && this.longusuario == null) {
      swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Datos incorrectos',
        showConfirmButton: false,
        timer: 1500
      });

    }
    if (this.longusuario != null && this.longtecnico == null && this.longcoordinador == null) {
    
       
      this.tomardato = this.NombreLogiadoUsuario.persona.nombres
      localStorage.setItem('nombreUsuario', this.tomardato);
      this.valorMenu = "1"
      localStorage.setItem('validaMenu', this.valorMenu);
      this.cedulaPersona = this.NombreLogiadoUsuario.persona.cedula
      localStorage.setItem('cedulaPersona', this.cedulaPersona);
      this.codigoUsuario = this.NombreLogiadoUsuario.codUsuario
      localStorage.setItem('codigoUsuario', this.codigoUsuario);
      window.location.href = "/ListaCatalogoUsuario"
      
     }
     if ( this.longtecnico != null && this.longusuario == null &&  this.longcoordinador == null) {
      this.tomardato = this.NombreLogiadoTecnico.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "2"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoTecnico.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoTecnico.codTecnico
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"

    }
    if ( this.longcoordinador != null &&  this.longusuario == null && this.longtecnico == null  ) {
      this.tomardato = this.NombreLogiadoCoordinador.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "3"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoCoordinador.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoCoordinador.codCoordinador
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"
    }

    if ( this.longusuario != null && this.longcoordinador != null && this.longtecnico == null ) {
      const swalWithBootstrapButtons = swal.mixin({
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-primary'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Como deseas ingresar?',
        text: "Selecciona el tipo de usuario",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: '  Coordinador ',
        cancelButtonText: '  Usuario Normal  ',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          this.tomardato = this.NombreLogiadoCoordinador.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "3"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoCoordinador.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoCoordinador.codCoordinador
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"



        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === swal.DismissReason.cancel
        ) {
          this.tomardato = this.NombreLogiadoUsuario.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "1"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoUsuario.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoUsuario.codUsuario
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"
        }
      })

    }

    if (this.longtecnico != null && this.longusuario != null && this.longcoordinador == null) {
      const swalWithBootstrapButtons = swal.mixin({
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-primary'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Como deseas ingresar?',
        text: "Selecciona el tipo de usuario",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: '  Tecnico ',
        cancelButtonText: '  Usuario Normal  ',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          this.tomardato = this.NombreLogiadoTecnico.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "2"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoTecnico.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoTecnico.codTecnico
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"


        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === swal.DismissReason.cancel
        ) {
          this.tomardato = this.NombreLogiadoUsuario.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "1"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoUsuario.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoUsuario.codUsuario
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"
        }
      })

    }
    if (this.longtecnico != null && this.longusuario != null && this.longcoordinador != null) {
      const swalWithBootstrapButtons = swal.mixin({
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-primary',
          denyButton: 'btn btn-primary'

        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Como deseas ingresar?',
        text: "Selecciona el tipo de usuario",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: '  Tecnico ',
        cancelButtonText: '  Usuario Normal  ',
        denyButtonText: '  Coordinador  ',
        reverseButtons: true,
        showDenyButton: true
      }).then((result) => {
        if (result.isConfirmed) {
          this.tomardato = this.NombreLogiadoTecnico.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "2"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoTecnico.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoTecnico.codTecnico
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"


        } else if (
          /* Read more about handling dismissals below */

          result.dismiss === swal.DismissReason.cancel
        ) {
          this.tomardato = this.NombreLogiadoUsuario.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "1"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoUsuario.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoUsuario.codUsuario
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"
        } else if (result.isDenied) {
          this.tomardato = this.NombreLogiadoCoordinador.persona.nombres
          localStorage.setItem('nombreUsuario', this.tomardato);
          this.valorMenu = "3"
          localStorage.setItem('validaMenu', this.valorMenu);
          this.cedulaPersona = this.NombreLogiadoCoordinador.persona.cedula
          localStorage.setItem('cedulaPersona', this.cedulaPersona);
          this.codigoUsuario = this.NombreLogiadoCoordinador.codCoordinador
          localStorage.setItem('codigoUsuario', this.codigoUsuario);
          window.location.href = "/ListaCatalogoUsuario"


        }
      }

      )

    }
  }
  consultaValidacionUserPasword() {

    this.loginServicio.loginUsuario(this.username, this.password).then((data) => {
      console.log("Usuario", data);
      this.longusuario = data;
      this.NombreLogiadoUsuario = data
      
      this.loginServicio.loginTecnico(this.username, this.password).then((data) => {
        console.log("Tecnico", data);
        this.longtecnico = data;
        this.NombreLogiadoTecnico = data

       
        this.loginServicio.loginCoordinador(this.username, this.password).then((data) => {
          console.log("coordinador", data);
          this.longcoordinador = data;
          this.NombreLogiadoCoordinador = data
         
          this.validarUsuariosMultiples()

        })
       
      })
   
    })
  }
  

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

}
