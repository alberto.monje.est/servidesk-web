import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCatalogosUsuarioComponent } from './lista-catalogos-usuario.component';

describe('ListaCatalogosUsuarioComponent', () => {
  let component: ListaCatalogosUsuarioComponent;
  let fixture: ComponentFixture<ListaCatalogosUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCatalogosUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCatalogosUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
