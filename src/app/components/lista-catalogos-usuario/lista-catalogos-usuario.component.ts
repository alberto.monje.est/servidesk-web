import { Component, OnInit } from '@angular/core';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { Catalogo } from '../../models/catalogo';

@Component({
  selector: 'app-lista-catalogos-usuario',
  templateUrl: './lista-catalogos-usuario.component.html',
  styleUrls: ['./lista-catalogos-usuario.component.css']
})
export class ListaCatalogosUsuarioComponent implements OnInit {

  catalogos:Catalogo[]
  constructor(private catalogoService:CatalogoService) { }

  ngOnInit() {
    this.obtenerCatalogos()
  }

  obtenerCatalogos(){
    this.catalogoService.getAllCatalogo().subscribe(data=>{
      this.catalogos=data;
    })
  }

}
