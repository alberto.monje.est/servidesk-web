import { Component, OnInit } from '@angular/core';
import { PreguntasService } from 'src/app/services/preguntas.service';
import { Preguntas } from '../../models/preguntas';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {

  form:FormGroup
  preguntas:Preguntas[]
  valores:number[]=[1,2,3,4,5]
  suma:number=0
  constructor(private preguntaService:PreguntasService,private formBuilder:FormBuilder) { }

  ngOnInit() {
    this.obtenerPreguntas()
    this.buildForm()
    
  }

  buildForm(){
    this.form= this.formBuilder.group({
      respuesta:['',[Validators.required]]
    })
  }

  obtenerPreguntas(){
    this.preguntaService.getAllPreguntas().subscribe(data=>{
      this.preguntas=data
      console.log(this.preguntas)
    })
  }
   
  obtenerValores(valor:number){
    let v = Number(valor)
    this.suma = this.suma+v 
    console.log(this.suma)
    const cantidad = this.preguntas.length
    const puntuacion =this.suma/cantidad
    console.log(puntuacion)

  }
  

}
