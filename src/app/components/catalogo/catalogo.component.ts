import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { Catalogo } from '../../models/catalogo';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { ServicioService } from '../../services/servicio.service';
import { Servicio } from '../../models/servicio';


@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {

  form:FormGroup;
  catalogos:Catalogo[]
  exist:Catalogo;
  servicios:Servicio[]

  constructor(
    private formBuilder:FormBuilder,
    private catalogoService:CatalogoService) { 
    this.buildForm();
  }

  ngOnInit() {
    this.obtenerCatalogos()
  }

  buildForm(){
    this.form=this.formBuilder.group({
      categoria:['',[Validators.required]],
      descripcion:['',[Validators.required]]

    });
  }

  
  save(event:Event){
    event.preventDefault();
    const value=this.form.value;
    const catalogo:Catalogo={categoria:value.categoria,descripcion:value.descripcion,estado:true}
    //Valida si el objeto ingresado existe e identificar si guardar o actualizar
    if(this.exist==null||this.exist==undefined){
      this.guardarCatalogo(catalogo)
    }else{
      this.actualizarCatalogo(this.exist.codCatalogo,catalogo)
     
    }
    

  }
  //Metodo para obtener todos los catalogos y permitir listarlos
  obtenerCatalogos(){
    this.catalogoService.getAllCatalogo().subscribe(data=>{
      this.catalogos=data;
    })
  }

  //Metodo para eliminar el catalogo
  eliminarCatalogo(codCatalogo:number){
    
    this.catalogoService.deleteCatalogo(codCatalogo).subscribe(data=>{
      this.ngOnInit()
    },err=>{
      this.showDependencia()
    })
    
   
  }

  //Metodo para obtener los valores por codigo de catalogo y cargar al formulario
  obtenerValores(categoria:string){
    this.catalogoService.getCatalogoByCategoria(categoria).subscribe(data=>{
      this.exist=data
      this.form=this.formBuilder.group({
        categoria:[data.categoria,[Validators.required]],
        descripcion:[data.descripcion,[Validators.required]]
  
      });
    })
  }

  guardarCatalogo(catalogo:Catalogo){
    this.catalogoService.addNewCatalogo(catalogo).subscribe((data:Catalogo)=>{
      if(data.categoria!=null){
       this.showExitoso()
        this.ngOnInit()
        this.buildForm()
      }else{
        this.showAdvertencia()
      }
    },err=>{
      console.log(err.error)
      this.showError()
    })
  }

  actualizarCatalogo(codCatalogo:number,catalogo:Catalogo){
    this.catalogoService.putCatalogo(codCatalogo,catalogo).subscribe(data=>{
      this.showActualizacion()
      this.exist=null
      this.buildForm()
      this.ngOnInit()
    })
  }
  showConfirmacion(codCatalogo:number){
    Swal.fire({
      title: '¿Estas seguro de eliminar este catalogo?',
      text: "Si eliminas no podras revertir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí,eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.eliminarCatalogo(codCatalogo)
        Swal.fire(
          'Eliminado!',
          'El catalogo ha sido eliminado',
          'success'
        )
      }
    })
  }

  showError(){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Surgió un error inesperado'
    })
  }

  showExitoso(){
    Swal.fire({
      icon: 'success',
      title: 'Se guardó con exito',
      showConfirmButton: false,
      timer: 2000
    })
  }

  showAdvertencia(){
    Swal.fire({
      icon: 'error',
      title: 'La categoria ingresada ya existe,revise e intente de nuevo',
    })
  }
  
  showActualizacion(){
    Swal.fire({
      icon: 'success',
      title: 'Datos Actualizados',
      showConfirmButton: false,
      timer: 2000
    })
  }

  showDependencia(){
    Swal.fire({
      icon: 'error',
      title: 'No se puede eliminar este catalogo',
      text: 'Existen servicios con este catalogo'
    })
  }
}
