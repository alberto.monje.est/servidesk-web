import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarPreguntasComponent } from './registrar-preguntas.component';

describe('RegistrarPreguntasComponent', () => {
  let component: RegistrarPreguntasComponent;
  let fixture: ComponentFixture<RegistrarPreguntasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarPreguntasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarPreguntasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
