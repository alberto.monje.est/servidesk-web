import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PreguntasService } from 'src/app/services/preguntas.service';
import { Preguntas } from '../../models/preguntas';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-registrar-preguntas',
  templateUrl: './registrar-preguntas.component.html',
  styleUrls: ['./registrar-preguntas.component.css']
})
export class RegistrarPreguntasComponent implements OnInit {

  form:FormGroup;
  preguntas:Preguntas[]
  exist:Preguntas
  constructor(private formBuilder:FormBuilder,private preguntaService:PreguntasService) { 
    this.buildForm()
  }

  ngOnInit() {
    this.obtenerPreguntas()
  }

  buildForm(){
    this.form=this.formBuilder.group({
      pregunta:['',[Validators.required]]
    });
  }

  save(event:Event){
    event.preventDefault();
    const value=this.form.value;
    const pregunta:Preguntas={pregunta:value.pregunta}
    console.log(pregunta)
    if(this.exist==null||this.exist==undefined){
      this.guardarPregunta(pregunta)
     
    }else{
      this.actualizarPregunta(this.exist.codPregunta,pregunta)
    }
  }

  guardarPregunta(pregunta:Preguntas){
    this.preguntaService.addNewPregunta(pregunta).subscribe(data=>{
      if(data.pregunta==null){
        this.showAdvertencia()
      }else{
        this.showExitoso()
        console.log(data)
        this.exist=null
        this.buildForm()
        this.ngOnInit()
      }
    },err=>{
      this.showError()
      console.log(err.error)
    });
  }

  obtenerPreguntas(){
    this.preguntaService.getAllPreguntas().subscribe(data=>{
      console.log(data)
      this.preguntas=data
    })
  }

  actualizarPregunta(codPregunta:number,pregunta:Preguntas){
    this.preguntaService.updatePregunta(codPregunta,pregunta).subscribe(data=>{
      console.log(data)
      this.showActualizacion()
      this.exist=null
      this.buildForm()
      this.ngOnInit()
    },err=>{
      console.log(err.error)
      this.showError()
    });

  }

  

  eliminarPregunta(codPregunta:number){
    this.preguntaService.deletePregunta(codPregunta).subscribe(data=>{
      console.log(data)
      this.ngOnInit()
    })
  }

  obtenerValores(codPregunta:number){
    this.preguntaService.getPreguntaByCodigo(codPregunta).subscribe(data=>{
      console.log(data)
      this.exist=data
      this.form=this.formBuilder.group({
        pregunta:[data.pregunta,[Validators.required]],
      });
    })
  }

  showConfirmacion(codPregunta:number){
    Swal.fire({
      title: '¿Estas seguro de eliminar esta pregunta?',
      text: "Si eliminas no podras revertir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí,eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.eliminarPregunta(codPregunta)
        Swal.fire(
          'Eliminado!',
          'La pregunta ha sido eliminada',
          'success'
        )
      }
    })
  }

  showError(){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Surgió un error inesperado'
    })
  }

  showExitoso(){
    Swal.fire({
      icon: 'success',
      title: 'Se guardó con exito',
      showConfirmButton: false,
      timer: 2000
    })
  }

  showAdvertencia(){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'La pregunta ingresada ya existe,revise e intente de nuevo'
    })
  }
  
  showActualizacion(){
    Swal.fire({
      icon: 'success',
      title: 'Datos Actualizados',
      showConfirmButton: false,
      timer: 2000
    })
  }

}
