import { Component, OnInit } from '@angular/core';
import { Servicio } from '../../models/servicio';
import { ServicioService } from '../../services/servicio.service';
import { FormGroup, FormBuilder, Validators, Form } from '@angular/forms';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { Catalogo } from '../../models/catalogo';

@Component({
  selector: 'app-lista-servicios-usuario',
  templateUrl: './lista-servicios-usuario.component.html',
  styleUrls: ['./lista-servicios-usuario.component.css']
})
export class ListaServiciosUsuarioComponent implements OnInit {

  servicios: Servicio[]
  filtroCategorias:FormGroup
  catalogos: Catalogo[]
  catalogo: Catalogo
  page:number=1
  constructor( private servicioService: ServicioService, private formBuilder: FormBuilder, private catalogoService: CatalogoService) {
    this.buildCategoriaForm()
   }

  
  ngOnInit() {
    this.obtenerServicios()
    this.obtenerCatalogos()
  }

  buildCategoriaForm(){
    this.filtroCategorias=this.formBuilder.group({
      categoriaseleccionada:['', [Validators.required]]
    })
  }
  getCatalogo(codCatalogo: number) {
    this.catalogoService.getCatalogoByCodigo(codCatalogo).subscribe(data => {
      this.catalogo = data
    }, err => {
      console.log(err.error)
    })
  }

  obtenerServicios() {
    this.servicioService.getAllServicios().subscribe(data => {
      this.servicios = data
    })
  }
  obtenerCatalogos() {
    this.catalogoService.getAllCatalogo().subscribe(data => {
      this.catalogos = data
    })
  }

  obtenerServiciosPorCategoria(event:Event){
    event.preventDefault();
    const value=this.filtroCategorias.value
    if(value.categoriaseleccionada==0){
      this.obtenerServicios()
    }
    this.servicioService.getServicioByCategoria(value.categoriaseleccionada).subscribe(data=>{
      this.servicios=data;
    },
    err=>{
      console.log(err.error)
    })
  }
}
