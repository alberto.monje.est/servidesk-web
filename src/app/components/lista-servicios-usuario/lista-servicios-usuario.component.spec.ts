import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaServiciosUsuarioComponent } from './lista-servicios-usuario.component';

describe('ListaServiciosUsuarioComponent', () => {
  let component: ListaServiciosUsuarioComponent;
  let fixture: ComponentFixture<ListaServiciosUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaServiciosUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaServiciosUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
