import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Coordinador } from 'src/app/models/coordinador';
import { Persona } from 'src/app/models/persona';
import { CoordinadorService } from 'src/app/services/coordinador.service';
import { PersonaService } from 'src/app/services/persona.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-coordinador',
  templateUrl: './registrar-coordinador.component.html',
  styleUrls: ['./registrar-coordinador.component.css']
})
export class RegistrarCoordinadorComponent implements OnInit {
  dataPersona: Persona;
  dataCoordinador: Coordinador;

  cedula: string;
  nombres: string;
  direccion: string;
  telefono: string;
  email: string;
  contrasena: string;
  codCoordinador: number
  ///vereficar el estatus
  valor1
  valor
  //trae el localstage de login
  dato
  codigocoordinador
  cedulapersona

  coordinador: Coordinador;
  persona: Persona;

   //validar la cedula
  tomarvalor

  constructor( public modal: NgbActiveModal, private route: ActivatedRoute,  private router: Router ,private personaService: PersonaService, private coordinadorservicio: CoordinadorService) {

    this.dato = localStorage.getItem("validaMenu");
    this.cedulapersona = localStorage.getItem("cedulaPersona");
    this.codigocoordinador = localStorage.getItem("codigoUsuario");
    console.log(this.codigocoordinador)
   }

  ngOnInit() { 
    this.coordinador
    this.persona
    if (this.dato == '3') {
      

      this.coordinadorservicio.getCoordinador(this.codigocoordinador).subscribe(data2 => {
        console.log(data2, "valor")
        this.email = data2.email
        this.contrasena = data2.password
        this.codCoordinador = data2.codCoordinador
        this.cedula = data2.persona.cedula
        this.nombres = data2.persona.nombres
        this.telefono = data2.persona.telefono
        this.direccion = data2.persona.direccion
      }, error => console.log(error));
    }
  }
  onSubmit() {

    this.modal.close('Yes');

  }
  salir() {
    this.onSubmit()
    this.email = ''
    this.contrasena = ''
    this.cedula = ''
    this.nombres = ''
    this.telefono = ''
    this.direccion = ''
  }

  validarCedula(cedula) {
    if (cedula.length == 10) {
      //Obtenemos el digito de la region que sonlos dos primeros digitos
      var digito_region = cedula.substring(0, 2);

      //Pregunto si la region existe ecuador se divide en 24 regiones
      if (digito_region >= 1 && digito_region <= 24) {
        // Extraigo el ultimo digito
        var ultimo_digito = cedula.substring(9, 10);

        //Agrupo todos los pares y los sumo
        var pares =
          parseInt(cedula.substring(1, 2)) +
          parseInt(cedula.substring(3, 4)) +
          parseInt(cedula.substring(5, 6)) +
          parseInt(cedula.substring(7, 8));

        //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
        var numero11 = cedula.substring(0, 1);
        var numero111 = numero11 * 2;
        var numero9 = 0;
        var numero3 = 0;
        var numero5 = 0;
        var numero7 = 0;
        var numero1 = 0;
        if (numero111 > 9) {
          numero1 = numero111 - 9;
        }

        var numero22 = cedula.substring(2, 3);
        var numero222 = numero22 * 2;
        if (numero222 > 9) {
          numero3 = numero222 - 9;
        }

        var numero33 = cedula.substring(4, 5);
        var numero333 = numero33 * 2;
        if (numero333 > 9) {
          numero5 = numero333 - 9;
        }

        var numero44 = cedula.substring(6, 7);
        var numero444 = numero44 * 2;
        if (numero444 > 9) {
          numero7 = numero444 - 9;
        }

        var numero55 = cedula.substring(8, 9);
        var numero555 = numero55 * 2;
        if (numero555 > 9) {
          numero9 = numero555 - 9;
        }

        var impares = numero1 + numero3 + numero5 + numero7 + numero9;

        //Suma total
        var suma_total = pares + impares;

        //extraemos el primero digito
        var primer_digito_suma = String(suma_total).substring(0, 1);

        //Obtenemos la decena inmediata
        var decena = (parseInt(primer_digito_suma) + 1) * 10;

        //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
        var digito_validador = decena - suma_total;

        //Si el digito validador es = a 10 toma el valor de 0
        if (digito_validador == 10) var digito_validador = 0;

        //Validamos que el digito validador sea igual al de la cedula
        if (digito_validador == ultimo_digito || ultimo_digito == 0) {
          console.log('la cedula:' + cedula + ' es correcta');
          this.tomarvalor = 1;

        } else {
          console.log('la cedula: ' + ultimo_digito + ' es incorrecta');
         
        }
      } else {
        // imprimimos en consola si la region no pertenece
        this.tomarvalor = 2;
       
        console.log('Esta cedula no pertenece a ninguna region');
      }
    } else {
      this.tomarvalor = 3;
      //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
     
      console.log('Esta cedula tiene menos de 10 Digitos');
    }
  }

  Acciboton() {
    if (this.dato == 3) {
      this.modificar();
    } else {
      if (this.cedula != undefined) {
        this.validarCedula(this.cedula);
        if (this.nombres != undefined) {
          if (this.direccion != undefined) {
            if (this.telefono != undefined) {
              if (this.email != undefined) {
                if (this.contrasena != undefined) {
                  console.log(this.nombres ," nombresss")
                  if (this.tomarvalor != 1) {
                   
                    swal.fire({
                      position: 'center',
                      icon: 'warning',
                      title: 'Cedula Incorrecta',
                      showConfirmButton: false,
                      timer: 1500
                    });

                  } else {
                    this.GurdarlosUsuario();
                  }
                } else {
                  swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'El campo contraseña es requerido',
                    showConfirmButton: false,
                    timer: 1500
                  });
                }
              } else {
                swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'El campo email es requerido',
                  showConfirmButton: false,
                  timer: 1500
                });
              }
            } else {
              swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'El campo telefono es requerido',
                showConfirmButton: false,
                timer: 1500
              });
            }
          } else {
            swal.fire({
              position: 'center',
              icon: 'warning',
              title: 'El campo direccion es requerido',
              showConfirmButton: false,
              timer: 1500
            });
          }
        } else {
          swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'El campo nombre es requerido',
            showConfirmButton: false,
            timer: 1500
          });
        }
      } else {
        swal.fire({
          position: 'center',
          icon: 'warning',
          title: 'El campo cedula es requerido',
          showConfirmButton: false,
          timer: 1500
        });
      }
    }

  }

  GurdarlosUsuario() {
    let dataPersona: Persona;
    let dataCoordinador: Coordinador;

    let valor: any
    let valor2: any


    dataPersona = {
      cedula: this.cedula,
      nombres: this.nombres,
      telefono: this.telefono,
      direccion: this.direccion,
    };


    dataCoordinador = {
      codCoordinador: 8000,
      email: this.email,
      password: this.contrasena,
      persona: dataPersona,
    };
    console.log(dataPersona);
    console.log(dataCoordinador);

    try {
      this.personaService.getcreatePersona(dataPersona).then((data) => {
        console.log(data.status)
        valor = data.status
        console.log("hi", valor)


        if (valor == 200) {
          try {
            this.coordinadorservicio.createCoordinador(dataCoordinador).then((data) => {
              valor2 = data.status



              if (valor2 == 200) {
                swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Se guardo correctamente',
                  showConfirmButton: false,
                  timer: 1500
                })
                this.onSubmit();
                console.log(valor2, "error")
                valor2 = 0;
              } else {
                swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'Datos incorrectos',
                  showConfirmButton: false,
                  timer: 1500
                });

              }
            })
          } catch (error) {
            console.log(JSON.stringify(error), " error al crear una persona")

          }
        } else {


        }

      })
    } catch (error) {
      console.log(JSON.stringify(error), " error al crear una persona")

    }
  }

  modificar() {
    let dataPersona: Persona;
    let dataCoordinador: Coordinador;

    let valor: any
    let valor2: any


    dataPersona = {
      cedula: this.cedula,
      nombres: this.nombres,
      telefono: this.telefono,
      direccion: this.direccion,
    };
    dataCoordinador = {
      codCoordinador: this.codCoordinador,
      email: this.email,
      password: this.contrasena,
      persona: dataPersona,
    };

    this.personaService.editarPersona(this.cedula, dataPersona)
      .then((data) => {
        this.valor = data.status

        console.log(this.valor
          , "estatus")

        this.coordinadorservicio.editarCoordinador(this.codCoordinador, dataCoordinador)
          .then((data) => {
            this.valor1 = data.status

            console.log(this.valor1
              , "estatus2")

            if (this.valor == 200 && this.valor1 == 200) {
              swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Se guardo correctamente',
                showConfirmButton: false,
                timer: 1500
              })
              this.onSubmit();
            } else {

              swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'Error al guardar',
                showConfirmButton: false,
                timer: 1500

              })
            }
          }) 
          
      })
  }
}


