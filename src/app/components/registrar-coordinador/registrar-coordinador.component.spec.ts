import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarCoordinadorComponent } from './registrar-coordinador.component';

describe('RegistrarCoordinadorComponent', () => {
  let component: RegistrarCoordinadorComponent;
  let fixture: ComponentFixture<RegistrarCoordinadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarCoordinadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarCoordinadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
