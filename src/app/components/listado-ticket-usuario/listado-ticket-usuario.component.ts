import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Estado } from 'src/app/models/estado';
import { Ticket } from 'src/app/models/ticket';
import { Usuario } from 'src/app/models/usuario';
import { SolucionService } from 'src/app/services/solucion.service';
import { TicketService } from 'src/app/services/ticket.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listado-ticket-usuario',
  templateUrl: './listado-ticket-usuario.component.html',
  styleUrls: ['./listado-ticket-usuario.component.css']
})
export class ListadoTicketUsuarioComponent implements OnInit {

  estado: any = [];
  usuarioLogiado
  mensaje
  ticket: any=[];
  Tickets:Ticket[]
  page:number = 1;
  constructor(private ticketService: TicketService,  private router: Router, private solucionServicio:SolucionService,) {
    this.usuarioLogiado=localStorage.getItem("codigoUsuario");
   }

  ngOnInit() {
 
    this.obtenerCatalogos();
  }

  obtenerCatalogos(){
    this.ticketService.getTicketsByUser(this.usuarioLogiado).then(data=>{
      this.ticket=data;
      console.log(data)
    })
  }

 

  openAlerta() {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Aceptar',
      showConfirmButton: false,
      timer: 1500
    });

  }

  obtenercodTicket(valor: any){
    localStorage.setItem("codigoTickets",valor)
    this.router.navigateByUrl("/generarTicket")
    
    
    }

alertanotificacion(codigoTicke:number){
  this.solucionServicio.getTicket(codigoTicke).subscribe((data=>{
    this.mensaje= data[0].descripcion
  
  console.log(codigoTicke," Numero de ticket modificado")
  Swal.fire({
    title: 'Notificacion',
    text: 'Solucion: '+ data[0].descripcion,
    icon: 'success',
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'OK'
  }).then((result) => {
    if (result.isConfirmed) {
     this.ticketService.putTicketEstado(5,codigoTicke ).subscribe((data)=>{
      this.ngOnInit();
      console.log(data,"si entro");
     })
     
    }
  })
}))
}


rechazar(codigoTicke:number){
  
  
  console.log(codigoTicke," Numero de ticket modificado")
  Swal.fire({
    title: 'Desea rechazar este ticket?',
    text: 'Confirme: ',
    icon: 'warning',
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'OK'
  }).then((result) => {
    if (result.isConfirmed) {
     this.ticketService.putTicketEstado(3,codigoTicke ).subscribe((data)=>{
      this.ngOnInit();
      console.log(data,"si entro");
     })
     
    }
  })

}



}
