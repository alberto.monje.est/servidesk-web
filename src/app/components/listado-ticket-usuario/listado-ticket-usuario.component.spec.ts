import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoTicketUsuarioComponent } from './listado-ticket-usuario.component';

describe('ListadoTicketUsuarioComponent', () => {
  let component: ListadoTicketUsuarioComponent;
  let fixture: ComponentFixture<ListadoTicketUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoTicketUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoTicketUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
