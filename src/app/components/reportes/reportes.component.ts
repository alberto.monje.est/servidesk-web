import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReporteGeneral } from 'src/app/models/reporteDto';
import { ReporteService } from 'src/app/services/reporte.service';
import { Estado } from '../../models/estado';
import { EstadoService } from '../../services/estado.service';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  form:FormGroup
  listadatos:ReporteGeneral[]
  estados:Estado[]
  page:number=1
  constructor(private formBuilder:FormBuilder,private reporteService:ReporteService,private estadoService:EstadoService) { 
    this.buildForm()
  }

  ngOnInit() {
    this.getEstados()
  }

  buildForm(){
    this.form=this.formBuilder.group({
      fechainicial:[''],
      fechafinal:[''],
      estado:['']
    })
  }

  getReportes(event:Event){
    event.preventDefault()
    const valores=this.form.value
    if(valores.fechainicial==''||valores.fechafinal==''){
      this.showDependencia()
    }else{
      this.reporteService.getAllReportes(valores.fechainicial,valores.fechafinal,valores.estado).subscribe(data=>{
        this.listadatos=data
        const tamano = this.listadatos.length
        if(tamano==0){
          this.showAdvertencia()

        }
        
      })
    }
    
  }

  getEstados(){
    this.estadoService.getAllEstados().subscribe(data=>{
      this.estados=data
    })
    
  }

  validacionLista(){
    if(this.listadatos==undefined||this.listadatos==null){
      this.showAdvertencia()
    }
  }

  exportarCsv(){
    if(this.form.value.fechainicial==''||this.form.value.fechafinal==''||this.form.value.estado==''){
      this.showDependencia()
    }else{
      const tamano = this.listadatos.length
      if(tamano==0){
        this.showAdvertencia()
      }else{
        this.downloadFile()
      } 
    }
    
  }

  downloadFile(){
    const fecha = formatDate(new Date(), 'dd-MM-yyyy HH:mm:ss', 'en');
    const filename=`reporte_servidesk_${fecha}.csv`
    const valores = this.form.value
    this.reporteService.exportCsv(valores.fechainicial,valores.fechafinal,valores.estado).subscribe(response=>{
      this.manageFile(response,filename)
      this.showDownload()
    })
  }

  manageFile(response:any,filename:string){
    const dataType =response.type
    const binaryData=[]
    binaryData.push(response)

    const fileUrl=window.URL.createObjectURL(new Blob(binaryData,{type:dataType}))
    const downloadLink=document.createElement('a')
    downloadLink.href=fileUrl
    downloadLink.setAttribute('download',filename)
    document.body.appendChild(downloadLink)
    downloadLink.click()

  }

  showDownload(){
    Swal.fire({
      icon:'success',
      title:'Documento Generado Exitosamente',
      timer:2000
    })
  }

 
  showAdvertencia() {
    Swal.fire({
      icon: 'warning',
      title: 'Aún no existen datos'
    })
  }

  showDependencia() {
    Swal.fire({
      icon: 'warning',
      title: 'No se puede generar el archivo',
      text: 'Ingrese los campos necesarios',
    })
  }

}
