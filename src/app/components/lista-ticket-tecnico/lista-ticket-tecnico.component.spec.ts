import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTicketTecnicoComponent } from './lista-ticket-tecnico.component';

describe('ListaTicketTecnicoComponent', () => {
  let component: ListaTicketTecnicoComponent;
  let fixture: ComponentFixture<ListaTicketTecnicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTicketTecnicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTicketTecnicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
