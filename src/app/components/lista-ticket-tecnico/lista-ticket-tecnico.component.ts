import { Component, OnInit } from '@angular/core';
import { Ticket } from 'src/app/models/ticket';
import { TicketService } from '../../services/ticket.service';

import { FormGroup, FormBuilder, Form, Validators } from '@angular/forms';
import { Estado } from '../../models/estado';
import { EstadoService } from '../../services/estado.service';
import { Tecnico } from '../../models/tecnico';
import { ViaComunicacion } from '../../models/viaComunicacion';
import { stringify } from '@angular/compiler/src/util';
import { Solucion } from '../../models/solucion';
import { SolucionService } from '../../services/solucion.service';
import { PersonaService } from '../../services/persona.service';
import { Persona } from '../../models/persona';
import { TecnicoService } from '../../services/tecnico.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-lista-ticket-tecnico',
  templateUrl: './lista-ticket-tecnico.component.html',
  styleUrls: ['./lista-ticket-tecnico.component.css']
})
export class ListaTicketTecnicoComponent implements OnInit {

  form: FormGroup;
  ticketsPorTecnico: any = [];
  ticketPorId: any[];
  tickets: any[];
  tick
  estados: Estado[];
  ticketFull : any =[];
  tecnicosNivel2: Tecnico;
  codtecnico;
  versolucion: number
  solucion: Solucion
  solucionFull : any= [];  
  fecha = formatDate(new Date(), 'yyyy-MM-dd', 'EN')
  fechaSolucion = this.fecha
  ticket : Ticket
  codCoordinador: number    
  nivel : number
  p:number=1
  page:number=1;
  EstadoCod : number;
  estadoSeleccion : number;
  selectEstado: number;
  constructor(private TicketService: TicketService, private EstadoService: EstadoService, private TecnicoService: TecnicoService ,
    private SolucionService: SolucionService,
    private formBuilder: FormBuilder) {
    this.buildForm();
    this.codtecnico = localStorage.getItem('codigoUsuario');
  }

  ngOnInit() {
    this.listarTicketsByIdTecnico();
    this.listaTecnicoNivel2();
  }
  buildForm() {
    this.form = this.formBuilder.group({
      nroTicket: [''],
      fechaAsignacion: [''],
      servicio: [''],
      categoria: [''],
      descripcion: [''],
      cedulaU: [''],
      nombreU: [''],
      telefonoU: [''],
      correoU: [''],
      estadoSelect: [''],
      fechaS: [''],
      descripcionS:[''],
      descripcionSolucion: ['',[Validators.required]]
    });
  }
  
  listarTicketsByIdTecnico() {
    this.TicketService.getTicketByIdTecnico(this.codtecnico).subscribe(data => {
      if(data.length == 0 ){
          this.showTicket();
      }else{
        this.ticketsPorTecnico = data
        this.nivel = this.ticketsPorTecnico[0].tecnico.nivel
      }     
    }
    )
  }
  cambiarEstado() {
    this.selectEstado = this.estadoSeleccion;
    
    this.EstadoCod=this.estadoSeleccion;
    
    if(this.selectEstado == 1){
      this.EstadoCod == 4
      this.TicketService.putTicketEstado(4, this.form.value.nroTicket).subscribe(data => {
        this.listarTicketsByIdTecnico();
        this.showEstado();
        this.TicketService.confirmarTicket(this.ticketFull.codticket).subscribe(data =>{
          })
       
      })
    }else if(this.selectEstado == 2){
      this.EstadoCod == 5
      /*this.TicketService.putTicketEstado(5, this.form.value.nroTicket).subscribe(data => {
        console.log (this.EstadoCod);
        this.listarTicketsByIdTecnico();
        this.showEstado();
       
      })*/
    }
      
 
  }
  
  obtenerValores(nroTicket: number, codCoordinador:number) {
    this.selectEstado=0;
    this.SolucionService.getTicketById(nroTicket).then(data =>{
      this.solucionFull= data[0];
      //if(this.solucionFull.codSolucion = 4)
    })
    this.TicketService.getTicketById(nroTicket).then(data => {

       this.ticket = data[0].codticket
        this.ticketFull= data[0];
       this.codCoordinador= codCoordinador
       
      if(data[0].estado.codEstado==4 ){
        this.EstadoCod = 1
      }else if(data[0].estado.codEstado==5){
        this.EstadoCod = 2
      }else if(data[0].estado.codEstado!=5 && data[0].estado.codEstado!=4){
        this.EstadoCod = 0
      }
      this.form = this.formBuilder.group({
       // nroTicket: this.ticket[0].codticket,
        nroTicket: data[0].codticket,
        fechaAsignacion:  formatDate(data[0].fechaAsignacion,'yyyy-MM-dd  HH:mm','EN') ,
        servicio: data[0].servicio.tipo.nombre,
        categoria:data[0].servicio.catalogo.categoria,
        descripcion:data[0].descripcionTicket,
        cedulaU:data[0].usuario.persona.cedula,
        nombreU: data[0].usuario.persona.nombres,
        telefonoU:data[0].usuario.persona.telefono,
        correoU: data[0].usuario.email,
        estadoSelect: [this.EstadoCod],
        fechaS :[''],
        descripcionS : [''],
        descripcionSolucion : ['']
      })
    })  
    
  }   
  guardarSolucion( event: Event) {
    
    const solution: Solucion = {  descripcion: this.form.value.descripcionSolucion, fechafinalizacion: this.fecha, ticket: this.ticketFull }
    
    if(this.ticket != null || this.ticket != undefined ){
            
      this.SolucionService.crearSolucion(solution).subscribe((data: Solucion) => {
        this.showExitoso();
        
        this.TicketService.confirmarTicket(this.ticketFull.codticket).subscribe(data =>{
        
        })
      })
    } 
  }
  listaTecnicoNivel2(){
    this.TecnicoService.obtenerTecnicoNivel(2).subscribe(data =>{
      this.tecnicosNivel2 = data
    })
  }
  escalarTecnico(codTecnico : number){
    this.TicketService.escalarTicket(codTecnico,this.codCoordinador, this.form.value.nroTicket).subscribe(data =>{
      this.listarTicketsByIdTecnico();
    })
  }
  reasignarTicket(codTicket: number){
    this.TicketService.reasignarTicket(codTicket).subscribe(data =>{
    this.TicketService.putCambiarEstado(1,codTicket).then(data =>{
      this.listarTicketsByIdTecnico();
    })
  })
  }
  showAsignar(codTicket: number){
    Swal.fire({
      title: '¿Estas seguro de reasignar el ticket?',
      text: "Si reasigna no podra revertir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí,reasignar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.reasignarTicket(codTicket)
        Swal.fire(
          'Reasignado!',
          'El ticket a sido reasignado',
          'success'
        )
      }
    })
  }
  showEscalar(codTecnico : number){
    Swal.fire({
      title: '¿Estas seguro de escalar este ticket?',
      text: "Si escala el ticket no podra revertir el proceso!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí, escalar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.escalarTecnico(codTecnico)
        Swal.fire(
          'Reescalado!',
          'El ticket a sido escalado',
          'success'
        )
      }
    })
  }
  showExitoso(){
    Swal.fire({
      icon: 'success',
      title: 'Se guardó con exito',
      showConfirmButton: false,
      timer: 2000
    })
  }
  showError(){
    Swal.fire({
      icon: 'error',
      title: 'No se registro la solucion del ticket',
      text: 'Surgió un error inesperado, intentelo nuevamente'
    })
  }
  showEstado(){
    Swal.fire({
      icon: 'success',
      title: 'Estado cambiado',
      showConfirmButton: false,
      timer: 2000
    })
  }
  showTicket(){
    Swal.fire({
      icon: 'success',
      text: 'No tienes tickets asignados',
      showConfirmButton: false,
      timer: 2000
    })
  }
  

}
