import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, Form } from '@angular/forms';
import { ServicioService } from '../../services/servicio.service';
import { Servicio } from '../../models/servicio';
import { CatalogoService } from '../../services/catalogo.service';
import { Catalogo } from '../../models/catalogo';
import { Tipo } from '../../models/tipo';
import { TipoService } from 'src/app/services/tipo.service';
import { PrioridadService } from 'src/app/services/prioridad.service';
import { Prioridad } from '../../models/prioridad';
import { Criticidad } from '../../models/criticidad';
import { CriticidadService } from '../../services/criticidad.service';
import Swal from 'sweetalert2/dist/sweetalert2.js'


@Component({
  selector: 'app-solicitar-servicio',
  templateUrl: './solicitar-servicio.component.html',
  styleUrls: ['./solicitar-servicio.component.css']
})
export class SolicitarServicioComponent implements OnInit {

  form: FormGroup
  servicios: Servicio[]
  catalogos: Catalogo[]
  tipos: Tipo[]
  prioridades: Prioridad[]
  criticidades: Criticidad[]
  tipo: Tipo
  criticidad: Criticidad
  prioridad: Prioridad
  catalogo: Catalogo
  exist: Servicio = null
  filtroCategorias:FormGroup
  page:number=1

  constructor(
    private servicioService: ServicioService,
    private catalogoService: CatalogoService,
    private tipoService: TipoService,
    private prioridadService: PrioridadService,
    private criticidadService: CriticidadService,
    private formBuilder: FormBuilder
  ) {
    this.buildForm()
    this.buildCategoriaForm()
  }

  ngOnInit() {
    this.obtenerServicios()
    this.obtenerCatalogos()
    this.obtenerTipos()
    this.obtenerPrioridades()
    this.obtenerCriticidades()

  }

  buildForm() {
    this.form = this.formBuilder.group({
      titulo: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      catalogo: ['', [Validators.required]],
      tipo: ['', [Validators.required]],
      prioridad: ['', [Validators.required]],
      criticidad: ['', [Validators.required]]
    })
  }
  buildCategoriaForm(){
    this.filtroCategorias=this.formBuilder.group({
      categoriaseleccionada:['', [Validators.required]]
    })
  }

  obtenerServicios() {
    this.servicioService.getAllServicios().subscribe(data => {
      this.servicios = data
    })
  }

  obtenerCatalogos() {
    this.catalogoService.getAllCatalogo().subscribe(data => {
      this.catalogos = data
    })
  }

  obtenerTipos() {
    this.tipoService.getAllTipo().subscribe(data => {
      this.tipos = data
    })
  }

  obtenerPrioridades() {
    this.prioridadService.getAllPrioridad().subscribe(data => {
      this.prioridades = data
    })
  }

  obtenerCriticidades() {
    this.criticidadService.getAllCriticidad().subscribe(data => {
      this.criticidades = data
    })
  }

  save(event: Event) {
    event.preventDefault()
    const valores = this.form.value
    this.getCatalogo(valores.catalogo)
    this.getTipo(valores.tipo)
    this.getPrioridad(valores.prioridad)
    this.getCriticidad(valores.criticidad)
    const servicio: Servicio = {
      titulo: valores.titulo, descripcion: valores.descripcion, fechaServicio: new Date(),
      catalogo: this.catalogo, tipo: this.tipo, prioridad: this.prioridad, criticidad: this.criticidad
    }
    if (this.exist == null) {
      if (servicio.catalogo == undefined || servicio.criticidad == undefined || servicio.tipo == undefined || servicio.prioridad == undefined) {
        this.showError()
      } else {
        this.guardarServicio(servicio)
        this.catalogo=null
        this.tipo=null
        this.prioridad=null
        this.criticidad=null
      }
    }else{
      if (servicio.catalogo == undefined || servicio.criticidad == undefined || servicio.tipo == undefined || servicio.prioridad == undefined) {
        this.showError()
      } else {
        this.actualizarServicio(this.exist.codServicio,servicio)
      }  
    }
  }

  getCatalogo(codCatalogo: number) {
    this.catalogoService.getCatalogoByCodigo(codCatalogo).subscribe(data => {
      this.catalogo = data
    }, err => {
      console.log(err.error)
    })
  }

  getTipo(codTipo: number) {
    this.tipoService.getTipo(codTipo).subscribe(data => {
      this.tipo = data
    }, err => {
      console.log(err.console.error)
    })
  }

  getPrioridad(codPrioridad: number) {
    this.prioridadService.getPrioridad(codPrioridad).subscribe(data => {
      this.prioridad = data
    }, err => {
      console.log(console.log(err.error))
    })
  }

  getCriticidad(codCriticidad: number) {
    this.criticidadService.getCriticidad(codCriticidad).subscribe(data => {
      this.criticidad = data
    }, err => {
      console.log(err.error)
    })
  }

  guardarServicio(servicio: Servicio) {
    this.servicioService.addNewServicio(servicio).subscribe(data => {
      this.showExitoso()
      this.ngOnInit()
      this.buildForm()
    }, err => {
      console.log(err.error)
      this.showError()
    })
  }

  eliminarServicio(codServicio: number) {
    this.servicioService.deleteServicio(codServicio).subscribe(data => {
      this.ngOnInit()
    },err=>{
      this.showErrorDependencia()
      console.log(err.error)
    })
  }

  cargarValores(codServicio: number) {
    this.servicioService.getServicioByCodigo(codServicio).subscribe(data => {
      this.exist = data
      this.form = this.formBuilder.group({
        titulo: [this.exist.titulo, [Validators.required]],
        descripcion: [this.exist.descripcion, [Validators.required]],
        catalogo: [this.exist.catalogo.codCatalogo, [Validators.required]],
        tipo: [this.exist.tipo.codTipo, [Validators.required]],
        prioridad: [this.exist.prioridad.codPrioridad, [Validators.required]],
        criticidad: [this.exist.criticidad.codCriticidad, [Validators.required]]
      })

    })
  }

  actualizarServicio(codServicio: number, servicio: Servicio) {
    this.servicioService.updateServicio(servicio, codServicio).subscribe(data => {
      this.showActualizacion()
      this.ngOnInit()
      this.buildForm()
      this.exist=null
    },err=>{
      console.log(err.error)
      this.showError()
    })
  }

  obtenerServiciosPorCategoria(event:Event){
    event.preventDefault();
    const value=this.filtroCategorias.value
    if(value.categoriaseleccionada==0){
      this.obtenerServicios()
    }
    this.servicioService.getServicioByCategoria(value.categoriaseleccionada).subscribe(data=>{
      this.servicios=data;
    },
    err=>{
      console.log(err.error)
      this.showError()
    })
  }
  //MENSAJES 

  showConfirmacion(codServicio: number) {
    Swal.fire({
      title: '¿Estas seguro de eliminar este servicio?',
      text: "Si eliminas no podras revertir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí,eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.eliminarServicio(codServicio)
        Swal.fire(
          'Eliminado!',
          'El servicio ha sido eliminado',
          'success'
        )
      }
    })
  }

  showError() {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Surgió un error inesperado'
    })
  }

  showExitoso() {
    Swal.fire({
      icon: 'success',
      title: 'Se guardó con exito',
      showConfirmButton: false,
      timer: 2000
    })
  }
  showActualizacion() {
    Swal.fire({
      icon: 'success',
      title: 'Datos Actualizados',
      showConfirmButton: false,
      timer: 2000
    })
  }

  showErrorDependencia() {
    Swal.fire({
      icon: 'error',
      title: 'No se puede eliminar el servicio',
      text:'Existen tickets con este servicio'
    })
  }

}
