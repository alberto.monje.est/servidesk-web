import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Ticket } from 'src/app/models/ticket';
import { TicketService } from 'src/app/services/ticket.service';
import { Persona } from 'src/app/models/persona';
import { Tecnico } from 'src/app/models/tecnico';
import { Observable } from 'rxjs';
import { TecnicoService } from 'src/app/services/tecnico.service';
import { Severidad } from 'src/app/models/severidad';
import { SeveridadService } from 'src/app/services/severidad.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-detalle-ticket-cordinador',
  templateUrl: './detalle-ticket-cordinador.component.html',
  styleUrls: ['./detalle-ticket-cordinador.component.css']
})
export class DetalleTicketCordinadorComponent implements OnInit {
  //datos a llenar en el html
  NumTicket: any = []
  cedulaUsario: any = []
  nombreUsuario: any = []
  categoria: any = []
  solucion: any = []
  sla: any = []
  severidad: any = []
  criticidad: any = []
  fechaAsiganacion: any = []
  nivelImpacto: any = []
  codigoCordinadorLogiado

  submitted = false;
  id;
  nommbre;
  ticket: Ticket;
  tecnico: Observable<Tecnico[]>;
  severida: Observable<Severidad[]>;


  
  //datos para clacular el SLA
  valorCritividad
  valorPrioridad
  valorSevridad
  valorsla
  cedulaTecnico
  constructor(public modal: NgbActiveModal, private route: ActivatedRoute, private router: Router,
    private ticketservicio: TicketService, private tecnicoservicio: TecnicoService, private severidadServicio: SeveridadService) {
      this.codigoCordinadorLogiado=localStorage.getItem("codigoUsuario");
     }

  ngOnInit() {

    this.ticket

    this.id = localStorage.getItem('valor')
    console.log(this.id + "valor id")
    this.ticketservicio.getTicket(this.id)
      .subscribe(data => {
        console.log(data)
        this.ticket = data;
        //llenando
        this.NumTicket = data[0].codticket
        this.cedulaUsario = data[0].usuario.persona.cedula
        this.nombreUsuario = data[0].usuario.persona.nombres
        this.categoria = data[0].servicio.catalogo.categoria
        this.sla = data[0].sla
        this.severidad = data[0].severidad
        this.criticidad = data[0].servicio.criticidad.nivelCriticidad
        this.fechaAsiganacion = data[0].fechaasiganacion
        this.nivelImpacto = data[0].severidad.codSeveridad
        this.valorCritividad=data[0].servicio.criticidad.valor;
        this.valorPrioridad=data[0].servicio.prioridad.tiempoRespuesta;
        this.cedulaTecnico=data[0].tecnico.codTecnico
       
      



      }, error => console.log(error));
    this.reloadData()
  }
  onSubmit() {
    this.submitted = true;
    this.modal.close('Yes');

  }

  reloadData() {
    this.tecnico = this.tecnicoservicio.getAllTecnicos();
    this.severida = this.severidadServicio.getAllSeveridad();

  }

  updateFechaAsignacion() {

    this.ticketservicio.updateFechaAsigancion(this.NumTicket, this.ticket)
      .subscribe(data =>
        console.log(data,"sda"),
        error => console.log(error));
        this.ticket = new Ticket();

  }

  asignarTecnico(codTecnico: number) {
    console.log(codTecnico, "datoTraido")
    
    this.ticketservicio.updateAsignarTecnico(codTecnico, 1, this.NumTicket, this.ticket)
      .subscribe(data =>
        console.log(data),
        error => console.log(error));
        this.ticket = new Ticket();
    console.log(codTecnico)
    this.ticketservicio.putCambiarEstado(2,this.NumTicket).then(data =>{})
    if(codTecnico != null){
      this.updateFechaAsignacion()
    }
    
  }

 
  updateSeveridad(codSeveridad: number) {
  
    this.ticketservicio.updateSeveridad(codSeveridad, this.NumTicket, this.ticket)
      .subscribe(data =>
        console.log(data),
        error => console.log(error));
       console.log(codSeveridad)

       this.severidadServicio.getSeveridad(codSeveridad).subscribe((data)=>{
        console.log(data)
        this.valorSevridad=data.valorSeveridad
        
      })
      let sla;
      sla=this.valorCritividad * this.valorPrioridad * codSeveridad
      this.valorsla= Math.trunc(sla)
      this.ticketservicio.CambioSla(this.valorsla,this.id).then((data)=>{

        
      })
      this.ngOnInit();
      this.ticket = new Ticket();
  }

  mensaje(){
  
    swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Se guardo correctamente',
      showConfirmButton: false,
      timer: 1500
    })
    this.onSubmit()
    this.ngOnInit();
  }
 
}
