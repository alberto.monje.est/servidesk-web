import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleTicketCordinadorComponent } from './detalle-ticket-cordinador.component';

describe('DetalleTicketCordinadorComponent', () => {
  let component: DetalleTicketCordinadorComponent;
  let fixture: ComponentFixture<DetalleTicketCordinadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleTicketCordinadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleTicketCordinadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
