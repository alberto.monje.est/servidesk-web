import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { Ticket } from 'src/app/models/ticket';
import { EstadoService } from 'src/app/services/estado.service';
import { TicketService } from 'src/app/services/ticket.service';
import { DetalleTicketCordinadorComponent } from '../detalle-ticket-cordinador/detalle-ticket-cordinador.component';
import { RegistrarUsuarioComponent } from '../registrar-usuario/registrar-usuario.component';

@Component({
  selector: 'app-lista-ticket-cordinador',
  templateUrl: './lista-ticket-cordinador.component.html',
  styleUrls: ['./lista-ticket-cordinador.component.css']
})
export class ListaTicketCordinadorComponent implements OnInit {

  ticket: any = [];
  estado: any = [];
  estadoTicket
  page:number = 1;
  page1:number = 1;
  page2:number = 1;
  filtroCategorias
  constructor(private ticketService: TicketService, private router: Router, private modalService: NgbModal, private estadoServicio: EstadoService) {
    this.estadoTicket = 1;
  }

  ngOnInit() {

    //this.obtenerticketCoordinador()
    this.ObtenerEstadoTicket()
    this.obtenerServiciosPorCategoria(this.estadoTicket);
  }
  ObtenerEstadoTicket(){
    this.estadoServicio.getAllEstados().subscribe((dato)=>{
      this.estado= dato
    })
  }
  obtenerticketCoordinador() {

    this.ticketService.getTicketsCoordinador().then(data => {
      this.ticket = data;
      console.log(data)
    }) 
   
  }

   obtenerServiciosPorCategoria(numero: any){
   
    this.ticketService.getTicketsByestado(numero).then(data=>{
      this.ticket = data;
    },
    err=>{
      console.log(err.error)
     
    })
  }

  Editarticket(valor) {
    const ref = this.modalService.open(DetalleTicketCordinadorComponent, { centered: true });
    localStorage.setItem('valor', valor);
    ref.result.then((yes) => {
      console.log("Yes Click");
      this.ngOnInit();

    },
      (cancel) => {
        console.log("Cancel Click");
        this.ngOnInit();
      })
      
  }

  segmentChanged(ev: any) {
    this.estadoTicket = ev.detail.value;
    
  }

  
}