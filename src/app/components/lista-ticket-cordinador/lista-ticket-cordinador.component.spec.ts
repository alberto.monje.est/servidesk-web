import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTicketCordinadorComponent } from './lista-ticket-cordinador.component';

describe('ListaTicketCordinadorComponent', () => {
  let component: ListaTicketCordinadorComponent;
  let fixture: ComponentFixture<ListaTicketCordinadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTicketCordinadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTicketCordinadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
