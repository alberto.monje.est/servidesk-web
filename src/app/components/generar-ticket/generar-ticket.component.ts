import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { Catalogo } from 'src/app/models/catalogo';
import { Coordinador } from 'src/app/models/coordinador';
import { Estado } from 'src/app/models/estado';
import { Servicio } from 'src/app/models/servicio';
import { Severidad } from 'src/app/models/severidad';
import { Ticket } from 'src/app/models/ticket';
import { Usuario } from 'src/app/models/usuario';
import { ViaComunicacion } from 'src/app/models/viaComunicacion';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { ConsumoImagenService } from 'src/app/services/consumo-imagen.service';
import { ServicioService } from 'src/app/services/servicio.service';
import { SeveridadService } from 'src/app/services/severidad.service';
import { TicketService } from 'src/app/services/ticket.service';
import { ViaComunicacionService } from 'src/app/services/via-comunicacion.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-generar-ticket',
  templateUrl: './generar-ticket.component.html',
  styleUrls: ['./generar-ticket.component.css']
})
export class GenerarTicketComponent implements OnInit {
  public archivos:any = [];
  public previsualizar:string;
  public listaIMAGENES:any=[];
  uploadText: any;
  downloadText: any;
  dataServicio: Servicio[] = [];
  dataCatalogo: Catalogo[] = [];
  dataSeveridad: Severidad[] = [];
  dataViaComunicacion: ViaComunicacion [] = [];
  fecha: string = new Date().toISOString();
  sendMsg: any = '';
  fileName: any;
  urlChange: any;

  // from lista tickets
  titulo: string;
  codTipo: any;

  // fileTransfer: FileTransferObject;
  hola
  //ng Variables
  codCatalogo: any;
  tituloCatalogo: any;
  codServicio
  tituloServicio: any;
  codSeveridad: any;
  nivelSeveridad: any;
  descripcionTicket: any;
  descripcionTicket1: any;
  codViaComunicacion: any;
  nombreViaComunicacion: any; 
  imgUrl: any = '';
  codTicket: any;
  editable: any;
  CodigoUsuarioLogiado
  //para para el achivo file
  archivo:File;
  photoSelected : string | ArrayBuffer;
  processing:boolean;
  uploadImage: string;
  img: String

  //datos para clacular el SLA
  valorCritividad
  valorPrioridad
  valorSevridad

  servicio: Observable<Servicio[]>;
  viaComunicacion: Observable<ViaComunicacion[]>;
  ticket: Observable<Ticket[]>;
  severidad: Observable<Severidad[]>;
  categoria :Observable<Catalogo[]>;
  codTipos

  obtenercodigoTicket
  constructor( private ticketservicio: TicketService, private consumo: ConsumoImagenService , private router: Router, private severidadServicio: SeveridadService, private viaComunicacionServicio :ViaComunicacionService,
    private servicioService: ServicioService , private categoriaservicio:CatalogoService) { 
      this.hola="seleccione"
      this.CodigoUsuarioLogiado=localStorage.getItem("codigoUsuario");
      this.obtenercodigoTicket= localStorage.getItem("codigoTickets")
      console.log(this.CodigoUsuarioLogiado, this.obtenercodigoTicket ," usuario traido ")
      this.codTipo=localStorage.getItem("codTipos")
    }

  ngOnInit() {
    this.reloadData()
    this.getCatalogos();
    this.getSeveridad();
    this.getViaComunicacion();
    //aqui se debe ingresar la ide generada por el usuario en su servicio
    let hola:any 
    if(this.obtenercodigoTicket !=null){
      this.getTicketById(this.obtenercodigoTicket)
    }

    
   
  }
  getTicketById(codTicket: any) {
    this.ticketservicio.getTicketById(codTicket).then((data) => {
      this.codCatalogo = data[0].servicio.catalogo.codCatalogo;
      this.tituloCatalogo = data[0].servicio.catalogo.categoria;
      this.codServicio = data[0].servicio.codServicio;
      this.tituloServicio = data[0].servicio.titulo;
      this.codSeveridad = data[0].severidad.codSeveridad;
      this.nivelSeveridad = data[0].severidad.nivelSeveridad;
      this.descripcionTicket = data[0].descripcionTicket;
      this.codViaComunicacion = data[0].viaComunicacion.codViaComunicacion;
      this.nombreViaComunicacion = data[0].viaComunicacion.nombre;
      this.imgUrl = data[0].url;
      this.codTipo = data[0].servicio.tipo.codTipo;
      this.titulo = data[0].servicio.tipo.nombre;
      this.valorCritividad = data[0].servicio.criticidad.valor;
      this.valorPrioridad = data[0].servicio.prioridad.tiempoRespuesta;
      this.valorSevridad = data[0].severidad.valorSeveridad;
      this.getServicios(this.codTipo, this.codCatalogo);
    });
  }

  getViaComunicacion() {
    this.viaComunicacionServicio.getViaComunicaciona().then((data: any) => {
      this.dataViaComunicacion = data;
    });
  }

  getSeveridad() {
    this.severidadServicio.getListSeveridad().then((data: any) => {
      this.dataSeveridad = data;
    });
  }

  getCatalogos() {
    this.categoriaservicio.getCatalogo().then((data: any) => {
      this.dataCatalogo = data;
    });
  }


  getServicios(tipo: any, catalogo: any) {
    this.servicioService.getServiciosByTipo(tipo, catalogo).subscribe(
      (data) => {
        this.dataServicio = data;
        console.log("cantidad de servicios: " + data.length);
        if (this.dataServicio.length == 0) {
          swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'No  existe servicios disponibles para esta categoria',
            showConfirmButton: false,
            timer: 2100
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }


  reloadData() {
    this.servicio = this.servicioService.getAllServicios();
    this.severidad = this.severidadServicio.getAllSeveridad();
    this.categoria= this.categoriaservicio.getAllCatalogo();
    this.viaComunicacion= this.viaComunicacionServicio.getAllViaComunicacion();
  }

  seleccionarSeveridad( CodigoServeridad){
    this.codSeveridad=CodigoServeridad;
     console.log(this.codSeveridad , " severidad")
     this.severidadServicio.getSeveridad(CodigoServeridad).subscribe((data)=>{
      console.log(data)
      this.valorSevridad=data.valorSeveridad
      console.log(this.valorCritividad," , ",this.valorPrioridad," , ",  this.valorSevridad )
    })
    localStorage.setItem("codSeveridad",  CodigoServeridad);
    let codSeveridad = localStorage.getItem("codSeveridad");
    console.log("codigo de severidad seleccionado/guardado "+codSeveridad);
    for (const data of this.dataSeveridad) {
      if (codSeveridad === data.codSeveridad.toString()) {
        localStorage.setItem("nivelSeveridad", data.nivelSeveridad);
        this.nivelSeveridad = localStorage.getItem("nivelSeveridad");
       
      } else {
        this.nivelSeveridad = data.nivelSeveridad;
       
      }
    }

  }

  seleccionarCatalogo(CodigoCategoria){
    // this.codCatalogo=CodigoCategoria
    // this.getServicios(this.codTipos,  this.codCatalogo);
    // console.log(this.codCatalogo," catagoria")
    localStorage.removeItem("tituloServicio");
    this.tituloServicio="";
    localStorage.removeItem("codServicio");
    localStorage.setItem("codCatalogo", CodigoCategoria);
    let codCatalogo = localStorage.getItem("codCatalogo");
    this.getServicios(this.codTipo, codCatalogo);
    console.log("codigo de catalogo seleccionado/guardado "+codCatalogo);
    for (const data of this.dataCatalogo) {
      if (codCatalogo === data.codCatalogo.toString()) {
        localStorage.setItem("tituloCatalogo", data.categoria);
        this.tituloCatalogo = localStorage.getItem("tituloCatalogo");
      } else {
        this.tituloCatalogo = data.categoria;
      }
    }
  }

  seleccionarServicio(CodigoServicio){
    this.codServicio=CodigoServicio
     console.log(this.codServicio," servicio")

    this.servicioService.getServicioByCodigo(CodigoServicio).subscribe((data)=>{
      console.log(data)
      this.valorCritividad=data.criticidad.valor;
    this.valorPrioridad=data.prioridad.tiempoRespuesta;

   
     
    })
    localStorage.setItem("codServicio", CodigoServicio);
    let codServicio = localStorage.getItem("codServicio");
    console.log("codigo de servicio seleccionado/guardado "+codServicio);
    for (const data of this.dataServicio) {
      if (codServicio === data.codServicio.toString()) {
        localStorage.setItem("tituloServicio", data.titulo);
        this.tituloServicio = localStorage.getItem("tituloServicio");
      } else {
        this.tituloServicio = data.titulo;
      }
    }
  }

  seleccionarViaComunicacion(CodigoViaComunicacion){
    // this.codViaComunicacion=CodigoViaComunicacion
    // console.log(this.codViaComunicacion," via")
    localStorage.setItem("codViaComunicacion", CodigoViaComunicacion);
    let codViaComunicacion = localStorage.getItem("codViaComunicacion");
    console.log("codigo de viaComunicacion seleccionado/guardado "+codViaComunicacion);
    for (const data of this.dataViaComunicacion) {
      if (codViaComunicacion === data.codViaComunicacion.toString()) {
        localStorage.setItem("nombreViaComunicacion", data.nombre);
        this.nombreViaComunicacion = localStorage.getItem("nombreViaComunicacion");
      } else {
        this.nombreViaComunicacion = data.nombre;
      }
    }
  }

  async createTicket() {
    let dataTicket: Ticket;
    let ticketStatus: any;
    let tiempoRespuestaSLA
    let valor
    tiempoRespuestaSLA=Math.trunc(this.valorCritividad*this.valorPrioridad*this.valorSevridad)
     if(this.obtenercodigoTicket !=null){
        valor =this.obtenercodigoTicket
     }else{
       valor=3
     }
    
      dataTicket = {
        codticket: valor,
        codServicio: this.codServicio,
        descripcionTicket: this.descripcionTicket,
        codSeveridad:this.codSeveridad,
        codViaComunicacion: this.codViaComunicacion,
        codUsuario:  this.CodigoUsuarioLogiado,
        url: this.imgUrl,
        fechaCreacion:this.fecha,
        estado: 1,
        codCoordinador: 1,
        codTecnico:1,
        fechaAsignacion:this.fecha,
        sla:tiempoRespuestaSLA,
        ticketEstado:true,
        codEstado:1,
        confirmacion:false
    
      };

  if(this.obtenercodigoTicket != null){

    this.ticketservicio.updateTicket(dataTicket).then((data)=>{

      console.log(data.status," status")
      if( data.status == 200){
        swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Se guardo correctamente',
          showConfirmButton: false,
          timer: 1500
        })
        this.router.navigateByUrl("/listadoTicketUsuario")
        localStorage.removeItem("codigoTickets")
      }
      })
  
  }else{

         this.ticketservicio.createTicket(dataTicket).then((data)=>{

        console.log(data.status," status")
        if( data.status == 200){
          swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Se guardo correctamente',
            showConfirmButton: false,
            timer: 1500
          })
          this.router.navigateByUrl("/listadoTicketUsuario")
          localStorage.removeItem("codigoTickets")
        }
        })
      }
     
  }

async uploadFile() {
  this.consumo.onClickSubir();
  setTimeout(async () => {
    this.imgUrl = localStorage.getItem('url');
    console.log('url de imagen ' + this.imgUrl);
  }, 3000);
}

onChange(fileChangeEvent: { target: { files: File[] } }) {
  this.archivo = fileChangeEvent.target.files[0];
  this.imgUrl = '';
  this.fileName = this.archivo.name;
  console.log(this.fileName);
  this.consumo.onChange(this.archivo);
  this.urlChange = 1;
}

//// carga imagen
async uploadTicket() {
  
  await this.uploadFile();
  
 
  if ( this.fileName != undefined ) {
   
    const Toast = swal.mixin({
      toast: true,
      position: 'center',
      showConfirmButton: false,
      timer: 3500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer)
        toast.addEventListener('mouseleave', swal.resumeTimer)
      }
    })


    Toast.fire({
      icon: 'success',
      title: 'Subiendo imagen'
    })
    setTimeout(async () => {
     await this.createTicket();
    }, 3500);
    localStorage.removeItem('url');
   
    this.ngOnInit()

  } else {
    setTimeout(async () => {
      await this.createTicket();
    }, 100);
   
    localStorage.removeItem('url');
    
    this.ngOnInit()
   


  }

}

  validarlosCampos() {
    if (this.codCatalogo != undefined) {
      if (this.codServicio != undefined) {
        if (this.codSeveridad != undefined) {
          if (this.codViaComunicacion != undefined) {
            if (this.descripcionTicket != undefined) {
              this.uploadTicket()
            } else {
              swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'El campo Descripcion es requerido',
                showConfirmButton: false,
                timer: 1500
              });
            }
          } else {
            swal.fire({
              position: 'center',
              icon: 'warning',
              title: 'El campo Via comunicacion es requerido',
              showConfirmButton: false,
              timer: 1500
            });
          }
        } else {
          swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'El campo Severidad es requerido',
            showConfirmButton: false,
            timer: 1500
          });
        }
      } else {
        swal.fire({
          position: 'center',
          icon: 'warning',
          title: 'El campo Servicio es requerido',
          showConfirmButton: false,
          timer: 1500
        });
      }
    } else {
      swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'El campo Catagoria es requerido',
        showConfirmButton: false,
        timer: 1500
      });
    }

  }

  cancelar() {
    localStorage.removeItem("codigoTickets")
    this.router.navigateByUrl("/ListaCatalogoUsuario")
  }

}
