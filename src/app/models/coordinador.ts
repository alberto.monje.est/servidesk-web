import { Persona } from './persona';
export interface Coordinador{
    codCoordinador?:number
    email:string
    password : string
    persona: Persona
}