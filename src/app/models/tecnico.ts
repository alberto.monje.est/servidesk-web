import { Persona } from './persona';
export interface Tecnico{

    codTecnico?: number
    email : string
    contrasena: string
    nivel : number
    persona : Persona
}