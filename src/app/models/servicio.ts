import { Catalogo } from './catalogo';
import { Prioridad } from './prioridad';
import { Criticidad } from './criticidad';
import { Tipo } from './tipo';
export interface Servicio{

    codServicio?:number
    titulo:string
    descripcion:string
    fechaServicio:Date
    catalogo:Catalogo
    prioridad:Prioridad
    tipo:Tipo
    criticidad:Criticidad
    
}